import { Component } from '@angular/core';
import {UserService} from "./services/user.service";
import {Router} from "@angular/router";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isMenuExpanded = false;

  constructor(private user : UserService, private router : Router){
  }
  toggleMenu(){
    this.isMenuExpanded = !this.isMenuExpanded;
  }
  logout(){
    this.user.logout();
    this.router.navigate(["/login"])

  }
  redirectTo(){
    this.router.navigate(["/mainpage"])
  }
  openSearching(){

  }

}
