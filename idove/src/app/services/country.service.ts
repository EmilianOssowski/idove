import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {CountryModel} from "../models/country.model";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpService, private user  :UserService, private popup : PopupService) { }

  countries : Map<number,CountryModel> = new Map<number, CountryModel>();

  countriesResults = new BehaviorSubject<Array<CountryModel>>(null);

  getCountryResults(query){
    this.http.getCountriesByQuery(this.user.getBearerToken(),query).subscribe((res : Array<CountryModel>) =>{
      this.countriesResults.next(res);
      for(let c of res){
        this.addCountry(c)
      }
    },err=>{
      this.popup.addErrorResponse(err.error)
    });
  }
  getCountryById(id) {
    return this.http.getCountryById(this.user.getBearerToken(), id)
  }
  addCountry(country){
    if(!this.countries.has(country.idCountry)){
      this.countries.set(country.idCountry, country);
    }
  }

  findCountryById(id){
    console.log('searching for country '+id);
      if(this.countries.has(id)){
        console.log('found country');
        return this.countries.get(id);
      }
      else{
        console.log('not found country, http request');
        this.http.getCountryById(this.user.getBearerToken(), id).subscribe((res) => {
          console.log('http respone');
          this.addCountry(res);
        },)
      }
    }
}
