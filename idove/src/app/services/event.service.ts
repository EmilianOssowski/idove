import {EventEmitter, Injectable} from '@angular/core';
import {EventModel} from "../models/event.model";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class EventService {
  events: any[] = [];
  options: any;
  eventsObs : BehaviorSubject<Array<EventModel>> = new BehaviorSubject([]);
  isEditing : EventEmitter<any> = new EventEmitter<any>();
  constructor(private http : HttpService, private user: UserService, private popup : PopupService) {
    this.options = {
      locale: 'pl',
      buttonText:
        {
          today:    'Dzisiaj',
          month:    'Miesiąc',
          week:     'Tydzień',
          day:      'Dzień',
          list:     'Lista'
        },
      header : {
        left:   'month,agendaWeek',
        center: 'title',
        right:  'today prev,next'
      },
      noEventsMessage: 'Brak planowanych wydarzeń',
      height: 650,
      handleWindowResize:true,
      defaultDate: new Date(),
      defaultView: 'month',
      eventClick : (e)=>{
        let event :EventModel = new EventModel(e.event.title, e.event.start, e.event.end, e.event.id);
        this.isEditing.emit(event)
      }
    };
  }


  getEvents(idBranch){
    this.http.getEventsByIdBranch(this.user.getBearerToken(),idBranch ).subscribe((res:Array<EventModel>)=>{
      this.eventsObs.next(res)
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }
  refreshEvents(){
    this.eventsObs.next(this.events)
  }
  addEvent(event, idBranch){
    this.http.addEvent(this.user.getBearerToken(), event,idBranch).subscribe(()=>{
      this.popup.addInfo('Dodano wydarzenie');
      this.getEvents(idBranch);
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
    this.events.push(event);
    this.refreshEvents()
  }
  editEvent(event, idBranch){
    this.http.putEvent(this.user.getBearerToken(), event, event.idEvent).subscribe(()=>{
      this.popup.addInfo('Dane wydarzenia zostały zmienione')
      this.getEvents(idBranch);
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }
  deleteEvent(idEvent, idBranch){

    this.http.deleteEvent(this.user.getBearerToken(), idEvent).subscribe(()=>{
      this.popup.addInfo('Wydarzenie zostało usunięte');
      this.getEvents(idBranch);
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }



  
}
