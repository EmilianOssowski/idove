import {EventEmitter, Injectable} from '@angular/core';
import {UserService} from "./user.service";
import {BranchModel} from "../models/branch.model";
import {HttpService} from "./http.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class BranchAdministratorService {

  branchList : Array<BranchModel> = [];
  isInitialized : boolean = false;

  constructor(private user : UserService, private http : HttpService, private popup : PopupService) {
    this.user.getBranchAdministratorInfo.subscribe(()=>{
      if(this.isInitialized==false) {
        this.getBranchList();
      }
    })
    this.user.deleteBranchAdministratorMenu.subscribe(()=>{
      this.branchList = [];
      this.isInitialized = false;
    })
  }

  initializeBranchAdministratorMenu : EventEmitter<BranchModel> = new EventEmitter();
  getBranchList(){
    this.branchList = [];
    this.http.getBranchListForAdministator(this.user.getBearerToken()).subscribe( (res : Array<BranchModel>)=>{
      this.branchList = res;
      if(this.branchList != null){
        this.branchList.forEach(b=>{
          this.initializeBranchAdministratorMenu.emit(b);
        })
      }
      this.isInitialized = true
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }

  getSectionList(idBranch){
    return this.http.getSectionsByIdBranch(this.user.getBearerToken(), idBranch);
  }

  addSection(section){
    return this.http.postSection(this.user.getBearerToken(), section);
  }
  editSection(section){
    return this.http.putSection(this.user.getBearerToken(), section, section.idSection);
  }
  deleteSection(idSection){
    return this.http.deleteSection(this.user.getBearerToken(), idSection);
  }
  
  getBranchById(idBranch){
    return this.http.getBranchDetails(this.user.getBearerToken(),idBranch );
  }
  getRegionByIdBranch(idBranch){
    return this.http.getRegionByIdBranch(this.user.getBearerToken(), idBranch);
  }
  getFanciersByIdBranch(idBranch){
    return this.http.getFanciers(this.user.getBearerToken(), idBranch);
  }
}
