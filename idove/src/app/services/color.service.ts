import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {ColorModel} from "../models/color.model";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  colorList = new BehaviorSubject<Array<ColorModel>>([]);
  constructor(private http : HttpService, private user : UserService, private popup : PopupService) {
    this.getColorList();
  }

  getColorList(){
    this.http.getColors(this.user.getBearerToken()).subscribe( (res : Array<ColorModel>) =>{
      this.colorList.next(res);
    },err=>{
      this.popup.addErrorResponse(err.error)
    });
  }

  getColorName(id) : string{
    let colors =  this.colorList.getValue();
    for (let val of colors) {
      if(val.idColor == id){
        return val.name;
      }

    }
  }
  getColorShortcut(id) : string{
    let colors =  this.colorList.getValue();
    for (let val of colors) {
      if(val.idColor == id){
        return val.shortcut;
      }

    }
  }
  getColor(id) : ColorModel{
    let colors =  this.colorList.getValue();
    for (let val of colors) {
      if(val.idColor == id){
        return val;
      }
    }
  }

}
