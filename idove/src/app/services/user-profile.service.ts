import { Injectable } from '@angular/core';
import {DovecoteModel} from "../models/dovecote.model";
import {BehaviorSubject} from "rxjs";
import {BranchModel} from "../models/branch.model";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {UserModel, UserProfileModel} from "../models/user.model";
import {AuctionsService} from "./auctions.service";
import {AuctionsInterface} from "../models/auction.model";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  //dane wyświetlane w profilu
  user : BehaviorSubject<UserModel> = new BehaviorSubject(null);
  dovecote : BehaviorSubject<DovecoteModel> = new BehaviorSubject(null);
  branch : BehaviorSubject<BranchModel>  = new BehaviorSubject(null);
  auctions : BehaviorSubject<Array<AuctionsInterface>> = new BehaviorSubject([]);
  //wyniki wyszukiwania po query
  userResults : BehaviorSubject<Array<UserModel>> = new BehaviorSubject([]);

  constructor(private http: HttpService, private userService : UserService, private auctionsService : AuctionsService, private popup : PopupService ) { }

  getUserProfile(idUser){
    this.http.getUserProfile(this.userService.getBearerToken(), idUser ).subscribe((res : UserProfileModel) =>{
      if(res ==null){
        this.user.next(null);
        this.dovecote.next(null);
        this.branch.next(null);
      }else{
        this.user.next(res.user);
        this.dovecote.next(res.dovecote);
        this.branch.next(res.branch);
        this.auctionsService.getAuctionsListByIdUser(this.user.getValue().idUser).subscribe((res : Array<AuctionsInterface>)=>{
          res.forEach(a=>{
            a.auction.createDate = new Date(a.auction.createDate)
          })
          this.auctions.next(res);
        },err=>{
          this.popup.addErrorResponse(err.error);
        })
      }
    })
  }
  getUserResults(query){
    this.http.getUsersByQuery(this.userService.getBearerToken(), query).subscribe((res: Array<UserModel>)=>{
      this.userResults.next(res)
    },err=>{
      this.popup.addErrorResponse(err.error)
    })


  }
}
