import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  emailRegex : RegExp= new RegExp('^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$');
  loginRegex : RegExp = new RegExp('^[a-zA-Z0-9_-]{3,16}$');
  passwordRegex : RegExp = new RegExp('^[a-zA-Z0-9_-]{6,18}$');
  text127Regex : RegExp = new RegExp('^[a-zA-Z0-9_-]{0,127}$');
  isUserDetailsValid(mail, password, firstName, lastName, address, city, telephoneNumber){

    return this.emailRegex.test(mail) &&
      this.passwordRegex.test(password)
  }
  isPasswordValid(password){
    return this.passwordRegex.test(password)
  }
}
