import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  getDateDisplay(date : Date){
    let res ='';
    res = res + date.getUTCFullYear().toString() +'-';
    if( date.getUTCMonth()<=9){
      res = res + '0'+(date.getUTCMonth()+1)+'-'
    }else{
      res = res + (date.getUTCMonth()+1)+'-'
    }
    if( date.getUTCDate()<=9){
      res = res + '0'+(date.getUTCDate())+' '
    }else{
      res = res + (date.getUTCDate())+' '
    }
    if( date.getUTCHours()<=8){
      res = res + '0'+(date.getUTCHours()+1)+':'
    }else if(date.getUTCHours()==23){
      res = res + '00:';
    }else{
      res = res + (date.getUTCHours()+1)+':'
    }
    if( date.getUTCMinutes()<=9){
      res = res + '0'+(date.getUTCMinutes())+':'
    }else{
      res = res + (date.getUTCMinutes())+':'
    }
    if( date.getUTCSeconds()<=9){
      res = res + '0'+(date.getUTCSeconds())
    }else{
      res = res + (date.getUTCSeconds())
    }
    return res;
  }
}
