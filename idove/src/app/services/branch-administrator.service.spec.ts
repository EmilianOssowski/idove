import { TestBed } from '@angular/core/testing';

import { BranchAdministratorService } from './branch-administrator.service';

describe('BranchAdministratorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BranchAdministratorService = TestBed.get(BranchAdministratorService);
    expect(service).toBeTruthy();
  });
});
