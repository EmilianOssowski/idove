import {Injectable} from '@angular/core';
import {BranchModel} from "../models/branch.model";
import {BehaviorSubject} from "rxjs";
import {SectionModel} from "../models/section.model";
import {PopupService} from "./popup.service";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {Router} from "@angular/router";
import {RegionModel} from "../models/region.model";
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  //metody i obiekty używane przy wyświetlaniu strony oddziału

  branchResults: BehaviorSubject<Array<BranchModel>> = new BehaviorSubject<Array<BranchModel>>(null);
  sections: BehaviorSubject<Array<SectionModel>> = new BehaviorSubject<Array<SectionModel>>(null);
  //używane przy stronie oddziału
  branch :  BranchModel = new BranchModel(null,null,null,null,null);
  region : RegionModel = new RegionModel(null,null,null);
  fanciers : BehaviorSubject<Array<UserModel>> = new BehaviorSubject<Array<UserModel>>([]);
  administrator : BehaviorSubject<UserModel> = new BehaviorSubject(null);

  constructor(private http : HttpService,private user : UserService, private popup: PopupService, private router : Router) {
  }

  getBranchDetails(idBranch) {
    this.http.getBranchDetails(this.user.getBearerToken(), idBranch).subscribe((res : BranchModel)=>{
      this.branch= res;
      this.getRegion(idBranch);
    },error1 => {
      this.popup.addErrorResponse(error1.error);
    })
  }
  getFanciers(idBranch){
    this.http.getFanciers(this.user.getBearerToken(), idBranch).subscribe((res: Array<UserModel>)=>{
      this.fanciers.next(res);
    },err =>{
      this.popup.addErrorResponse(err.error);
    })
  }

  getRegion(idBranch){
    this.http.getRegionByIdBranch(this.user.getBearerToken(), idBranch).subscribe((res : RegionModel)=>{
      this.region = res;
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }
  getBranchesByQuery(query) {
    this.http.getBranchesByQuery(this.user.getBearerToken(), query).subscribe((res : Array<BranchModel>) => {
      this.branchResults.next(res);
    },err=>{
      this.popup.addErrorResponse(err.error);
    });
  }

  getSections(idBranch){
    this.http.getSectionsByIdBranch(this.user.getBearerToken(),idBranch).subscribe((res : Array<SectionModel>) =>{
      this.sections.next(res)
    },err=>{
      this.popup.addErrorResponse(err.error);
    });
  }
  getBranch(id){
    return this.http.getBranchDetails(this.user.getBearerToken(),id)
  }
  getAdminstratorInfo(idBranch){
    this.http.getBranchAdministrator(this.user.getBearerToken(), idBranch).subscribe((res: UserModel)=>{
      this.administrator.next(res);
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }
  isApplicationButtonDisabled = false;
  applyForAdministrator(idBranch){
    this.isApplicationButtonDisabled = true;
    this.http.applyForAdmnistrator(this.user.getBearerToken(), idBranch).subscribe(()=>{

      this.popup.addInfo('Zgłoszenie zostało wysłane, decyzja o przyznaniu dostępu zostanie rozpatrzona w ciągu kilku dni', 'Informacja', 7000);
      this.isApplicationButtonDisabled = false;
    },err=>{
      this.popup.addErrorResponse(err.error);
      this.isApplicationButtonDisabled = false;
    })
  }
  openBranchSite(){
      if(this.user.branch == null){
        this.router.navigate(['/branch-missing'])
      }else{
        this.router.navigate(['/branch', this.user.branch.idBranch])
      }

  }



}
