import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {PigeonModel} from "../models/pigeon.model";
import {UserService} from "./user.service";
import {HttpService} from "./http.service";
import {PopupService} from "./popup.service";
import {DovecoteModel} from "../models/dovecote.model";

@Injectable({
  providedIn: 'root'
})
export class FancierService {
  //metody i obiekty używane przez użytkownika

  //lista gołębi użytkownika
  pigeons : BehaviorSubject<Array<PigeonModel>> = new BehaviorSubject<Array<PigeonModel>>([]);
  //dane gołębnika
  dovecote : DovecoteModel = new DovecoteModel(null,null,null,null,null);


  constructor(private http : HttpService, private user : UserService, private popup : PopupService) {
    this.getPigeons();
  }

  getPigeons(){
    this.http.getPigeons(this.user.getBearerToken()).subscribe( (res : Array<PigeonModel>) =>{
      this.pigeons.next(res);
    },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }

  addDovecote(dovecote){
    this.http.addDovecote(this.user.getBearerToken(), dovecote).subscribe(res =>{
      this.popup.addInfo('Dodano gołębnik');
      this.getDovecote()
    }, err=>{
      this.popup.addErrorResponse(err.error);
    });
  }

  addMarker : EventEmitter<any> =new EventEmitter<any>(null);
  clearMapEvent : EventEmitter<any> =new EventEmitter<any>(null);
  isInitial = true;

  getDovecote(){
    this.http.getDovecote(this.user.getBearerToken()).subscribe((res : DovecoteModel)=>{
      if(res != null) {
        this.dovecote.idDovecote = res.idDovecote;
        this.dovecote.address = res.address;
        this.dovecote.city = res.city;
        this.dovecote.latitude = res.latitude;
        this.dovecote.longitude = res.longitude;
        this.addMarker.emit(this.dovecote)
      }else{
        this.dovecote.idDovecote = null;
        this.dovecote.address = null;
        this.dovecote.city = null;
        this.dovecote.latitude = null;
        this.dovecote.longitude = null;
        this.clearMapEvent.emit()
      }
      this.isInitial = false;
    }, err => {
      this.popup.addErrorResponse(err.error);
      this.isInitial= false;
    })
  }
  editDovecote(dovecote){
    this.http.editDovecote(this.user.getBearerToken(), dovecote).subscribe(res =>{
      this.popup.addInfo('Zaktualizowano gołębnik');
      this.getDovecote();
    },err =>{
      this.popup.addErrorResponse(err.error)
    })
  }
  deleteDovecote(){
    this.http.deleteDovecote(this.user.getBearerToken()).subscribe(res=>{
      this.popup.addInfo('Usunięto dane gołębnika');
      this.getDovecote();
    },err =>{
      this.popup.addErrorResponse(err.error);
    })
  }

  joinSection(section, teamName = null){
    this.http.putFancier(this.user.getBearerToken(),section,teamName).subscribe(()=>{
      this.popup.addInfo('Przypisano do sekcji');
      this.user.refreshUserDetails();
    },error =>{
      this.popup.addErrorResponse(error.error)
    })
  }

  leaveSection(){
    this.http.putFancier(this.user.getBearerToken(), null, null).subscribe(res=>{
      this.popup.addInfo("Opuściłeś sekcję");
      this.user.refreshUserDetails();
    },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }

  getSection( idSection){
    return this.http.getSectionById(this.user.getBearerToken(), idSection)
  }

  addPigeon(pigeon){
    this.http.postPigeon(this.user.getBearerToken(), pigeon).subscribe(()=>{
      this.popup.addInfo('Dodano gołębia');
      this.getPigeons();
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }

  editPigeon(pigeon){
    this.http.putPigeon(this.user.getBearerToken(),pigeon).subscribe(()=>{
      this.popup.addInfo('Zmieniono dane gołębia');
      this.getPigeons();
    },err=>{
        this.popup.addErrorResponse(err.error);
    })
  }
  deletePigeon(pigeon : PigeonModel){
    this.http.deletePigeon(this.user.getBearerToken(), pigeon.idPigeon).subscribe(res =>{
      this.popup.addInfo('Usunięto gołębia '+ pigeon.ringNumber);
      this.getPigeons()
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }

  getBranch(){
    return this.http.getBranch(this.user.getBearerToken())
  }

  filterPigeonsByRingNumber(query){
    let pigeons = this.pigeons.getValue();
    let results : PigeonModel[] = [];
    for(let p of pigeons){
      if(p.ringNumber.includes(query.toUpperCase()))
        results.push(p);
    }
    return results;
  }
  getPigeonsByQuery(query, sex){
    return this.http.getPigeonsByQuery(this.user.getBearerToken(), query, sex);
  }
  getPigeonByIdPigeon(idPigeon){
     return this.http.getPigeonByIdPigeon(this.user.getBearerToken(),idPigeon)
  }
}
