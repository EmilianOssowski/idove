import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http:  HttpService, private popup : PopupService) { }

  isRegisterButtonDisabled = false;

  register(user){
    this.isRegisterButtonDisabled = true;
    this.http.register(user).subscribe((res :any) =>{
      this.popup.addInfo('Wysłano link aktywacyjny', 'Informacja', 5000);
      this.isRegisterButtonDisabled = false
    },error1 => {
        this.popup.addErrorResponse(error1.error);
      this.isRegisterButtonDisabled = false;
    });

  }
}
