import { Injectable } from '@angular/core';
import {UserService} from "./user.service";
import {HttpService} from "./http.service";
import {RegionModel} from "../models/region.model";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  region : RegionModel = new RegionModel(null,null,null);
  constructor(private user : UserService, private http: HttpService, private popup : PopupService) { }

  getRegionById(idRegion){
    this.http.getRegionById(this.user.getBearerToken(),idRegion).subscribe( (res : RegionModel) =>{
      this.region = res;
    },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }
  getRegionByIdBranch(idBranch){
    this.http.getRegionByIdBranch(this.user.getBearerToken(),idBranch).subscribe( (res : RegionModel) =>{
      this.region = res;
    },err => {
      this.popup.addErrorResponse(err.error)
    })
  }
}
