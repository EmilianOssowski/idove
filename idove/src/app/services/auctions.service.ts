import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {AuctionModel, AuctionsInterface} from "../models/auction.model";
import {UserService} from "./user.service";
import {HttpService} from "./http.service";
import {PopupService} from "./popup.service";

@Injectable({
  providedIn: 'root'
})
export class AuctionsService {
  auctionsList : BehaviorSubject<Array<AuctionsInterface>> = new BehaviorSubject<Array<AuctionsInterface>>([]);

  userAuctionsList : BehaviorSubject<Array<AuctionsInterface>> = new BehaviorSubject<Array<AuctionsInterface>>([]);
  constructor(private user : UserService, private http: HttpService, private popup : PopupService) {
    this.getAuctionsList();
  }
  getAuctionsList(){
    this.http.getAuctionsAll(this.user.getBearerToken()).subscribe((res: Array<AuctionsInterface>)=>{
      let auctions = [];
      for(let a of res){
        a.auction.createDate = new Date(a.auction.createDate);
        auctions.push(a);
      }
      this.auctionsList.next(auctions);
    },err =>{
      this.popup.addErrorResponse(err.error)
    })
  }
  getUserAuctionsList(){
    this.http.getAuctionsByToken(this.user.getBearerToken()).subscribe((res: Array<AuctionsInterface>)=>{
      let auctions = [];
      for(let a of res){
        a.auction.createDate = new Date(a.auction.createDate);
        auctions.push(a);
      }
      this.userAuctionsList.next(auctions);
    },err =>{
      this.popup.addErrorResponse(err.error)
    })
  }

  addAuction(auction : AuctionModel){
    this.http.postAuction(this.user.getBearerToken(), auction).subscribe(()=>{
      this.popup.addInfo('Aukcja została dodana');
      this.getAuctionsList()
      this.getUserAuctionsList()
    },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }
  editAuction(auction : AuctionModel){
    this.http.putAuction(this.user.getBearerToken(), auction).subscribe(()=>{
      this.popup.addInfo('Aukcja została zedytowana');
      this.getUserAuctionsList()
    },err=>{
      this.popup.addError(err.error)
    })
  };
  deleteAuction(idAuction){
    this.http.deleteAuction(this.user.getBearerToken(), idAuction).subscribe(()=>{
      this.popup.addInfo('Aukcja została usunięta');
      this.getUserAuctionsList()
    },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }
  getAuctionsListByIdUser(idUser){
    return this.http.getAuctionsByIdUser(this.user.getBearerToken(), idUser)
  }
}
