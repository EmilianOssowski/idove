import {EventEmitter, Injectable} from '@angular/core';
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class PopupService {

   forceLogout : EventEmitter<any> = new EventEmitter();
  constructor(private message: MessageService) {
  }

  addInfo(detail, summary = 'Informacja', life = 2000) {
    this.message.add({severity: 'info', summary: summary, detail: detail, life: life})
  }

  addError(detail, summary = 'Wystąpił błąd', life = 2000) {
    this.message.add({severity: 'error', summary: summary, detail: detail, life: life})
  }

  addErrorResponse(res) {
    if(res.error == "invalid_token") {
      this.addError('Brak autoryzacji. Nastąpiło autowylogowanie')
      this.forceLogout.emit();
    }else{
    switch (res) {
      case 'INCORRECT_PASSWORD': {
        this.addError('Błędne hasło')
        break;
      }
      case 'USER_NOT_EXISTS': {
        this.addError('Użytkownik nie istnieje')
        break;
      }
      case 'EMAIL_NOT_AVAILABLE': {
        this.addError('Podany adres e-mail jest niedostępny')
        break;
      }
      case 'EMAIL_NOT_DIFFER': {
        this.addError('Podany adres e-mail nie różni się od aktualnego')
        break;
      }
      case 'PIGEON_EXISTS': {
        this.addError('Gołąb o tym numerze obrączki już istnieje')
        break;
      }
      case 'USER_ALREADY_EXISTS': {
        this.addError('Użytkownik już istnieje')
        break;
      }
      case 'DOVECOTE_ALREADY_EXISTS': {
        this.addError('Gołębnik już istnieje')
        break;
      }
      case 'INVITATION_ALREADY_EXISTS': {
        this.addError('Zaproszenie zostało już wysłane')
        break;
      }
      case 'SECTION_ALREADY_EXISTS': {
        this.addError('Sekcja już istnieje')
        break;
      }
      case 'BRANCH_NOT_EXISTS': {
        this.addError('Oddział nie istnieje')
        break;
      }
      case 'INVITATION_NOT_EXISTS': {
        this.addError('Zaproszenie nie istnieje')
        break;
      }
      case 'CANNOT_FIND_ADMIN': {
        this.addError('Niestety, nie udało nam się znaleźć informacji o gołębiu w systemie iDove')
        break;
      }
      case 'CANNOT_SEND_INVITE': {
        this.addError('Nie można zaprosić samego siebie')
        break;
      }
      case  'invalid_grant': {
        this.addError('Błędne dane logowania')
        break;
      }


      default: {
        this.addError(res)
        console.log(res)
        break;
      }
    }
    }
  }
}
