import {EventEmitter, Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {PopupService} from "./popup.service";
import {UserDetailsModel} from "../models/userDetails.model";
import {UserModel} from "../models/user.model";
import {FancierModel} from "../models/fancier.model";
import {BranchModel} from "../models/branch.model";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  idUser : number;
  username : string;

  mail : string = null;
  fancier : FancierModel;
  userDetails : UserDetailsModel;

  //oddział do którego należy użytkownik
  branch : BranchModel = new BranchModel(null,null,null,null,null);
  isRestoringSession = false;
  isLoginButtonDisabled = false;

  isUserLogged : boolean = false;
  constructor(private http : HttpService, private cookie : CookieService, private router : Router, private popup : PopupService) {
    this.popup.forceLogout.subscribe(()=>{
      this.logout()
    })
  }


  login(username, password) {
    this.isLoginButtonDisabled = true;
    this.http.login(username, password).subscribe((res : any) =>{
      this.cookie.set('access_token', res.access_token, res.expires_in);
      this.getUserDetails();
      this.idUser = res.idUser;
      this.isLoginButtonDisabled = false;
    },(error : any) => {
        this.popup.addErrorResponse(error.error.error);
      this.isLoginButtonDisabled = false;
    })
  }

  getBranchAdministratorInfo: EventEmitter<boolean> = new EventEmitter();
  deleteBranchAdministratorMenu : EventEmitter<boolean> = new EventEmitter();
  getUserDetails(){
    this.http.getUserDetails(this.getBearerToken()).subscribe((res : UserModel)=>{
      this.mail = res.email;
      this.fancier = res.fancier;
      this.userDetails = res.userDetails;
      this.isUserLogged = true;
      this.isRestoringSession = false;
      this.getUserBranch();
      this.getBranchAdministratorInfo.emit();
      this.router.navigate(['/mainpage']);
    },error => {
      this.popup.addErrorResponse( error.error);
      this.isRestoringSession = false;
    })
  }


  refreshUserDetails(){
    this.http.getUserDetails(this.getBearerToken()).subscribe((res : UserModel)=>{
      this.mail = res.email;
      this.fancier = res.fancier;
      this.userDetails = res.userDetails;
      this.isUserLogged = true;
      this.getUserBranch();
    },error => {
      this.popup.addErrorResponse(error.error);
    })
  }
  logout(){
    this.idUser = null;
    this.username = null;
    this.mail = null;
    this.fancier= null;
    this.branch = null;
    this.userDetails = null;
    this.isUserLogged = false;
    this.cookie.deleteAll();
    this.cookie.delete('access_token');
    this.deleteBranchAdministratorMenu.emit();
    this.router.navigate(['/login'])
  }

  getBearerToken(){
    return this.cookie.get('access_token')
  }


  getUserBranch(){
    this.http.getBranch(this.getBearerToken()).subscribe((res : BranchModel)=> {
      this.branch = res;
      },err=>{
      this.popup.addErrorResponse(err.error)
    })
  }


}
