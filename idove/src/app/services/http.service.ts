import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {catchError} from "rxjs/operators";
import {Observable, throwError} from "rxjs";
import {PopupService} from "./popup.service";
import {SectionModel} from "../models/section.model";


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  // url: string = 'http://77.55.215.254:8080';
  url: string = 'http://localhost:8080';
  basicUsername: string = 'www-idove';
  basicPassword: string = 'dBaMF9ep';
  basicAuthorization: string = 'Basic d3d3LWlkb3ZlOmRCYU1GOWVw';


  //endpoints
  oauthToken = '/oauth/token';
  registerEndpoint = '/register';
  userEndpoint = '/user';
  changePasswordEndpoint = '/change-password';
  changeEmailEndpoint = '/change-email';
  resetPasswordEndpoint = '/reset-password';
  confirmEndpoint = '/confirm';
  dovecoteEndpoint = '/dovecote';
  countryEndpoint = '/country';
  colorEndpoint = '/color';
  branchEndpoint = '/branch';
  allEndpoint = '/all';
  sectionsEndpoint = '/sections';
  detailsEndpoint = '/details';
  fancierEndpoint = '/fancier';
  sectionEndpoint = '/section';
  regionEndpoint = '/region';
  friendEndpoint = '/friend';
  invitationEndpoint = '/invitation';
  acceptEndpoint = '/accept';
  rejectEndpoint = '/reject';
  calculatorEndpoint = '/calculator';
  branchesEndpoint = '/branches';
  pigeonEndpoint = '/pigeon';
  usersEndpoint = '/users';
  profileEndpoint = '/profile';
  lostPigeonEndpoint = '/lostPigeon';
  auctionEndpoint = '/auction';
  searchEndpoint = '/search';
  deleteEndpoint = '/delete';
  eventEndpoint = '/event';
  applicationEndpoint = '/application';



  constructor(private http: HttpClient, private router: Router, private popup : PopupService) {
  }

  login(username: string, password: string)  {
    let headers = new HttpHeaders({
      'Authorization': this.basicAuthorization,
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    body.set('grant_type', 'password');
    let qs = String(body);
    return this.http.post(this.url + this.oauthToken,
      qs, {headers: headers, withCredentials: true}).pipe(catchError(this.handleError));
  }
  getUserDetails(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });
    return this.http.get(this.url+this.userEndpoint, {
      headers : headers})
      .pipe(catchError(this.handleError))
  }


  register(user) {
    console.log(JSON.stringify(user));
    let headers = new HttpHeaders({
      'Authorization': this.basicAuthorization,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.registerEndpoint, JSON.stringify(user), {
      headers: headers,
      withCredentials: true
    }).pipe(catchError(this.handleError));
  }

  changePassword(token, oldPassword, newPassword){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.userEndpoint + this.changePasswordEndpoint, JSON.stringify({oldPassword: oldPassword,newPassword: newPassword}),
      {headers: headers}).pipe(catchError(this.handleError));
  }
  deleteAccount(token,password){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.userEndpoint + this.deleteEndpoint,JSON.stringify({password: password}), {headers : headers}).pipe(catchError(this.handleError));
  }
  confirmAccount(token){
    let headers = new HttpHeaders({ 'Content-type': 'application/json'});
    return this.http.get(this.url + this.registerEndpoint + this.confirmEndpoint, {params : new HttpParams().append('token', token),headers: headers })
      .pipe(catchError(this.handleError));
  }
  changeUserDetails(token, userDetails){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    })
    return this.http.put(this.url + this.userEndpoint, JSON.stringify( userDetails),{headers : headers}).pipe(catchError(this.handleError));
  }
  resetPassword(mail){
    return this.http.get(this.url + this.resetPasswordEndpoint,
      {params : new HttpParams().set('mail',mail)} ).pipe(catchError(this.handleError));
  }
  establishNewPassword(token , newPassword){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.changePasswordEndpoint,JSON.stringify(newPassword),
      {params : new HttpParams().set('token',token), headers: headers} ).pipe(catchError(this.handleError));
  }
  changeEmail(token, mail){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.userEndpoint + this.changeEmailEndpoint, JSON.stringify({newEmail : mail}), {headers: headers}).pipe(catchError(this.handleError));
  }
  establishNewEmail(token){
    return this.http.get(this.url + this.changeEmailEndpoint + this.confirmEndpoint, {params : new HttpParams().set('token',token)}).pipe(catchError(this.handleError));
  }

  getUserProfile(token, idUser){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url+this.friendEndpoint + this.profileEndpoint, {
      headers : headers,
      params : new HttpParams().set('id', idUser)})
      .pipe(catchError(this.handleError))
  }

  getUsersByQuery(token, query){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url+this.userEndpoint + this.searchEndpoint, {
      headers : headers,
      params : new HttpParams().set('name', query)})
      .pipe(catchError(this.handleError))
  }

  getDovecote(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url+this.dovecoteEndpoint, {headers : headers} ).pipe(catchError(this.handleError));
  }


  addDovecote(token, dovecote){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.dovecoteEndpoint, JSON.stringify(dovecote), {headers: headers} ).pipe(catchError(this.handleError));
  }
  editDovecote(token, dovecote){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url+this.dovecoteEndpoint , JSON.stringify(dovecote), {headers : headers}).pipe(catchError(this.handleError));
  }
  deleteDovecote(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.url+this.dovecoteEndpoint, {headers : headers}).pipe(catchError(this.handleError));
  }

  getCountriesByQuery(token,query){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.countryEndpoint ,{params : new HttpParams().set('name', query), headers : headers}).pipe(catchError(this.handleError));
  }
  getCountryById(token, id){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.countryEndpoint+ this.detailsEndpoint, {headers : headers, params: new HttpParams().set('id', id)})
      .pipe(catchError(this.handleError));
  }
  getColors(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.colorEndpoint , {headers: headers}).pipe(catchError(this.handleError));
  }
  getBranchesByQuery(token, query){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint +this.allEndpoint, {
      headers: headers,
      params : new HttpParams().set('shortcut', query)}).pipe(catchError(this.handleError));
  }
  getBranchDetails(token, idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint +this.detailsEndpoint, {
      headers: headers,
      params : new HttpParams().set('idBranch', idBranch)
    }).pipe(catchError(this.handleError));
  }

  getSectionsByIdBranch(token, idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint +this.sectionsEndpoint, {
      headers: headers,
      params : new HttpParams().set('idBranch', idBranch)
    }).pipe(catchError(this.handleError));
  }
  //numer oddziału od którego należy użytkownik
  getBranch(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint,
    {
      headers: headers
    }).pipe(catchError(this.handleError));
  }
  putFancier(token,section,teamName){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    let fancier = {teamName: teamName, section: section};
    return this.http.put(this.url + this.fancierEndpoint, JSON.stringify(fancier), {headers : headers} )
      .pipe(catchError(this.handleError));
  }

  getSectionById(token, idSection){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.sectionEndpoint, {headers : headers, params: new HttpParams().set('idSection', idSection)})
      .pipe(catchError(this.handleError));
  }

  getRegionById(token, idRegion){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.regionEndpoint, {headers:headers, params: new HttpParams().set('idRegion', idRegion)})
      .pipe(catchError(this.handleError));
  }
  getRegionByIdBranch(token, idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.regionEndpoint+ this.detailsEndpoint, {headers:headers, params: new HttpParams().set('idBranch', idBranch)})
      .pipe(catchError(this.handleError));
  }
  getFanciers(token, idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint+ this.usersEndpoint, {headers:headers, params: new HttpParams().set('idBranch', idBranch)})
      .pipe(catchError(this.handleError));
  }
  getBranchAdministrator(token, idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint+ this.calculatorEndpoint, {headers:headers, params: new HttpParams().set('idBranch', idBranch)})
      .pipe(catchError(this.handleError));
  }
  applyForAdmnistrator(token ,idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.branchEndpoint+ this.calculatorEndpoint + this.applicationEndpoint, {headers:headers, params: new HttpParams().set('id', idBranch)})
      .pipe(catchError(this.handleError));
  }
  //friends
  getFriends(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.friendEndpoint + this.allEndpoint, { headers : headers})
      .pipe(catchError(this.handleError));
  }

  deleteFriend(token, idFriend){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });
    return this.http.delete(this.url + this.friendEndpoint , {headers : headers, params : new HttpParams().set('friendId', idFriend)})
      .pipe(catchError(this.handleError));
  }

  //invitations
  getInvitations(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.invitationEndpoint + this.allEndpoint, { headers : headers})
      .pipe(catchError(this.handleError));
  }
  sendInvitation(token, idUser){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.invitationEndpoint, null, {headers : headers, params: new HttpParams().set('idGuest', idUser)})
      .pipe(catchError(this.handleError));
  }
  acceptInvitation(token, idInvitation){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });
    return this.http.get(this.url + this.invitationEndpoint +this.acceptEndpoint,{headers : headers, params : new HttpParams().set('id', idInvitation)} )
      .pipe(catchError(this.handleError));
  }
  rejectInvitation(token ,idInvitation){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
    });
    return this.http.get(this.url + this.invitationEndpoint +this.rejectEndpoint,{headers : headers, params : new HttpParams().set('id', idInvitation)} )
      .pipe(catchError(this.handleError));

  }
  getBranchListForAdministator(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.calculatorEndpoint + this.branchesEndpoint ,{headers : headers} )
      .pipe(catchError(this.handleError));
  }


  //section
  postSection(token, section : SectionModel){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.sectionEndpoint , JSON.stringify({idBranch : section.branchId, name : section.name}),{headers : headers} )
      .pipe(catchError(this.handleError));
  }
  putSection(token, section, idSection){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.sectionEndpoint , JSON.stringify({idBranch : section.branchId, name : section.name}),
      {headers : headers, params: new HttpParams().set('idSection', idSection)} )
      .pipe(catchError(this.handleError));
  }
  deleteSection(token, idSection){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.url + this.sectionEndpoint ,{headers : headers, params: new HttpParams().set('idSection', idSection)} )
      .pipe(catchError(this.handleError));
  }


  //pigeon
  getPigeons(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.pigeonEndpoint +this.allEndpoint ,{headers : headers})
      .pipe(catchError(this.handleError));
  }
  postPigeon(token, pigeon){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.pigeonEndpoint ,JSON.stringify(pigeon),{headers : headers})
      .pipe(catchError(this.handleError));
  }
  putPigeon(token, pigeon){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.pigeonEndpoint ,JSON.stringify(pigeon),{headers : headers, params : new HttpParams().set('id',pigeon.idPigeon)})
      .pipe(catchError(this.handleError));
  }
  deletePigeon(token, idPigeon){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.url + this.pigeonEndpoint ,{headers : headers, params : new HttpParams().set('id',idPigeon)})
      .pipe(catchError(this.handleError));
  }

  getPigeonByIdPigeon(token, idPigeon){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.pigeonEndpoint,{headers : headers, params: new HttpParams().set('id',idPigeon)})
      .pipe(catchError(this.handleError));
  }
  getPigeonsByQuery(token, query, sex){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.pigeonEndpoint +this.detailsEndpoint,{
      headers : headers,
      params: new HttpParams().set('ringNumber',query).append('sex', sex)})
      .pipe(catchError(this.handleError));
  }

  findLostPigeon(body){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.pigeonEndpoint +this.lostPigeonEndpoint,body, {
      headers : headers})
      .pipe(catchError(this.handleError));
  }

  //auctions
  getAuctionsAll(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.auctionEndpoint +this.allEndpoint,{headers : headers})
      .pipe(catchError(this.handleError));
  }


  getAuctionsByToken(token){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.auctionEndpoint,{headers : headers})
      .pipe(catchError(this.handleError));
  }


  getAuctionsByIdUser(token, idUser){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.auctionEndpoint + this.userEndpoint,{headers : headers,
      params: new HttpParams().set('id', idUser)})
      .pipe(catchError(this.handleError));
  }
  postAuction(token, auction){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.post(this.url + this.auctionEndpoint ,JSON.stringify(auction),{headers : headers})
      .pipe(catchError(this.handleError));
  }

  putAuction(token, auction){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.auctionEndpoint ,JSON.stringify(auction),{headers : headers})
      .pipe(catchError(this.handleError));
  }
  deleteAuction(token, idAuction){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.url + this.auctionEndpoint,{headers : headers, params: new HttpParams().set('id', idAuction)})
      .pipe(catchError(this.handleError));
  }
  //events
  getEventsByIdBranch(token , idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.url + this.eventEndpoint + this.branchEndpoint, {headers : headers,
      params : new HttpParams().set('id',idBranch)})
      .pipe(catchError(this.handleError));
  }
  addEvent(token, event,idBranch){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
     return this.http.post(this.url + this.eventEndpoint + this.branchEndpoint, JSON.stringify(event),
       {headers : headers, params: new HttpParams().set('id', idBranch)})
       .pipe(catchError(this.handleError));
  }
  putEvent(token, event, idEvent){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.put(this.url + this.eventEndpoint, JSON.stringify(event),
      {headers : headers, params: new HttpParams().set('id', idEvent)})
      .pipe(catchError(this.handleError));
  }
  deleteEvent(token, idEvent){
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    });
    return this.http.delete(this.url + this.eventEndpoint,
      {headers : headers, params: new HttpParams().set('id', idEvent)})
      .pipe(catchError(this.handleError));
  }


  //error handling
  isSessionExpired : EventEmitter<any> = new EventEmitter();
  private handleError(error: HttpErrorResponse) {
    //if(error.error.error == 'invalid_token') {
    //  this.logoutUser();
    //}

    return throwError(error || "Server error")
  }
  private logoutUser(){
    this.isSessionExpired.emit()
  }

}
