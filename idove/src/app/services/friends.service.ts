import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {FriendModel} from "../models/friend.model";
import {BranchService} from "./branch.service";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {forEach} from "@angular/router/src/utils/collection";
import {PopupService} from "./popup.service";
import {InvitationModel} from "../models/invitation.model";

@Injectable({
  providedIn: 'root'
})
export class FriendsService {
  friends : Array<FriendModel> = [];
  invitations : Array<InvitationModel> = [];
  constructor(private branch : BranchService, private http: HttpService, private user : UserService, private popup: PopupService) {}
  getFriends(){
    this.http.getFriends(this.user.getBearerToken()).subscribe((res : Array<FriendModel>) =>{
      this.friends = res;
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }
  getFriendsBranch(){
    this.friends.forEach(f =>{
      //pobranie danych o odziale dla każdego znajomego
      //this.branch.getBranch()
    })
  }
  sendInvitation(idUser){
    this.http.sendInvitation(this.user.getBearerToken(), idUser).subscribe( ()=>{
      this.popup.addInfo('Wysłano zaproszenie')
    },err =>{
      this.popup.addErrorResponse(err.error);
    })
  }
  getInvitations(){
    this.http.getInvitations(this.user.getBearerToken()).subscribe((res:Array<InvitationModel>)=>{
      this.invitations = res;
    },err=>{
      this.popup.addErrorResponse(err.error);
    })
  }

  acceptInvitation(idInvitation){
    this.http.acceptInvitation(this.user.getBearerToken(), idInvitation).subscribe(res =>{
      this.popup.addInfo('Użytkownik został dodany do listy znajomych');
      this.getFriends();
      this.getInvitations();
    },err =>{
      this.popup.addErrorResponse(err.error);
    })
  }
  rejectInvitation(idInvitation){
    this.http.rejectInvitation(this.user.getBearerToken(), idInvitation).subscribe(res =>{
      this.popup.addInfo('Zaproszenie zostało odrzucone');
      this.getFriends();
      this.getInvitations();
    },err =>{
      this.popup.addErrorResponse(err.error);
    })
  }
  deleteFriend(friend : FriendModel){
    this.http.deleteFriend(this.user.getBearerToken(), friend.user.idUser).subscribe(res =>{
      this.getFriends();
      this.getInvitations();
      this.popup.addInfo('Usunieto '+ friend.user.userDetails.firstName + " " + friend.user.userDetails.lastName + " z listy znajomych")
    },err =>{
      this.popup.addErrorResponse(err.error);
    })

  }
}
