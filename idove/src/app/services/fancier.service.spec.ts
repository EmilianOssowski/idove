import { TestBed } from '@angular/core/testing';

import { FancierService } from './fancier.service';

describe('FancierService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FancierService = TestBed.get(FancierService);
    expect(service).toBeTruthy();
  });
});
