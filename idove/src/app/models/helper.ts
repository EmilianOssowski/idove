import {OnInit} from "@angular/core";

export class Helper implements OnInit{

  pol: any;

  ngOnInit() {
    this.pol = {
      firstDayOfWeek: 0,
      dayNames: [ "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota","Niedziela"],
      dayNamesShort: ["Pn", "Wt", "Śr", "Czw", "Pt", "So", "Nd"],
      dayNamesMin: ["Pn", "Wt", "Śr", "Cz", "Pt", "So", "Nd"],
      monthNames: [ "January","February","March","April","May","June","July","August","September","October","November","December" ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
      today: 'Dzisiaj',
      clear: 'Wyczyść'
    };
  }


  static getDateString(date : Date){
    let res ='';
    res = res + date.getUTCFullYear().toString() +'-';
    if( date.getUTCMonth()<=9){
      res = res + '0'+(date.getUTCMonth()+1)+'-'
    }else{
      res = res + (date.getUTCMonth()+1)+'-'
    }
    if( date.getUTCDate()<=9){
      res = res + '0'+(date.getUTCDate())+'T'
    }else{
      res = res + (date.getUTCDate())+'T'
    }
    if( date.getUTCHours()<=8){
      res = res + '0'+(date.getUTCHours()+1)+':'
    }else if(date.getUTCHours()==23){
      res = res + '00:';
    }else{
      res = res + (date.getUTCHours()+1)+':'
    }
    if( date.getUTCMinutes()<=9){
      res = res + '0'+(date.getUTCMinutes())+':00'
    }else{
      res = res + (date.getUTCMinutes())+':00'
    }
    return res;
  }
}
