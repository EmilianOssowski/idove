export class AuctionModel{
  idAuction;
  fancier;
  title ;
  description;
  createDate : Date = new Date();

  constructor(idAuction, fancier, title, description, createDate = new Date()){
    this.idAuction = idAuction;
    this.fancier = fancier;
    this.title = title;
    this.description = description;
    this.createDate = createDate;
  }
}
export interface AuctionsInterface{
  auction: AuctionModel;
  idUser;
  firstName;
  lastName;
  email;
  telephoneNumber;
}
