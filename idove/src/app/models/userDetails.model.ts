export class UserDetailsModel {
  firstName ='';
  lastName ='';
  address ='';
  city ='';
  telephoneNumber ='';
  constructor(firstName,  lastName,  address,  city,  telephoneNumber){
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
    this.city = city;
    this.telephoneNumber = telephoneNumber;
  }
}
