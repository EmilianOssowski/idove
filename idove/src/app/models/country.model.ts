export class CountryModel{
  idCountry;
  name;
  shortcut;
  constructor (idCountry, name, shortcut){
    this.idCountry = idCountry;
    this.name = name;
    this.shortcut = shortcut;
  }
}
