export class SectionModel{
  idSection : number;
  branchId : number;
  name : string;
  constructor (idSection, branchId , name){
    this.idSection = idSection ;
    this.branchId = branchId ;
    this.name = name ;
  }
}
