export class RegionModel{
  idRegion;
  countryId;
  name;
  constructor(idRegion, countryId,name){
    this.idRegion = idRegion;
    this.countryId = countryId;
    this.name = name;
  }
}
