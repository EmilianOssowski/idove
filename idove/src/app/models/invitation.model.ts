import {UserModel} from "./user.model";

export class InvitationModel {
  idInvitation;
  host : UserModel;
  invitationDate;
  constructor(idInvitation, host, invitationDate){
    this.idInvitation = idInvitation;
    this.host = host ;
    this.invitationDate = invitationDate;
  }
}
