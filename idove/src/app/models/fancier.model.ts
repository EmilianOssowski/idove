import {SectionModel} from "./section.model";


export class FancierModel{
  idFancier : number = null;
  dovecoteId : number = null;
  section : SectionModel = null;
  teamName : string = null;
  constructor(idFancier, dovecoteId, section, teamName ){
    this.idFancier = idFancier ;
    this.dovecoteId = dovecoteId ;
    this.section = section ;
    this.teamName = teamName ;
  }
}
