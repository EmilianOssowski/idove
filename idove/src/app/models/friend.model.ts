
import {UserModel} from "./user.model";
import {BranchModel} from "./branch.model";
import {DovecoteModel} from "./dovecote.model";

export class FriendModel {
  idFriend;
  user: UserModel;
  branch : BranchModel;

  constructor(idFriend, user){
    this.idFriend = idFriend;
    this.user = user;
  }
}
