import {UserDetailsModel} from "./userDetails.model";
import {FancierModel} from "./fancier.model";
import {DovecoteModel} from "./dovecote.model";
import {BranchModel} from "./branch.model";

export class UserModel {
  idUser : number;
  email : string = '';
  isFancier;
  isCalculator;
  password : string = '';
  fancier : FancierModel  = new FancierModel(null, null, null, null);
  userDetails : UserDetailsModel = new UserDetailsModel(null,null,null,null,null);

  constructor(mail,  password,  firstName, lastName, address, city, telephoneNumber ){
    this.email = mail ;
    this.password = password ;
    this.userDetails = new UserDetailsModel(firstName, lastName, address, city, telephoneNumber ) ;
  }
}

export class UserProfileModel{
  user: UserModel = new UserModel(null,null,null,null,null,null,null);
  dovecote : DovecoteModel = new DovecoteModel(null,null,null,null,null);
  branch : BranchModel = new BranchModel(null,null,null,null,null);
}
