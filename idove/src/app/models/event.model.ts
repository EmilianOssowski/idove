export class EventModel
{
  idEvent : number;
  branchId : number;
  title= '';
  startTime : Date;
  endTime : Date;
  description='';

  constructor(title, start, end?, id?){
    this.idEvent = id;
    this.title = title;
    this.startTime = start;
    this.endTime = end;
  }

}
