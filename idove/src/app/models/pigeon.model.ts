export class PigeonModel{
  idPigeon;
  fancierId;
  countryId;
  colorId;
  branchNumber;
  yearbook : string;
  number;
  sex;
  ringNumber = null;
  isGMP = false;
  isMP = false;
  isIMP = false;
  description = null;
  isAlive = true;
  motherId = null;
  motherRingNumber = null;
  fatherId = null;
  fatherRingNumber = null;

  constructor(idPigeon, fancierId, countryId, colorId, branchNumber, yearbook, number, sex, motherId = null, fatherId = null){
    this.idPigeon = idPigeon;
    this.fancierId = fancierId;
    this.countryId = countryId;
    this.colorId = colorId;
    this.branchNumber = branchNumber;
    this.yearbook = yearbook;
    this.number = number;
    this.sex = sex;
    this.motherId  = motherId;
    this.fatherId  = fatherId;
  }
}
