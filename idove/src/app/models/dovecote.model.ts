export class DovecoteModel{
  idDovecote = null;
  address;
  city;
  latitude = null;
  longitude = null;
  constructor(idDovecote, address, city, latitude, longitude){
    this.idDovecote = idDovecote;
    this.address = address;
    this.city = city;
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
