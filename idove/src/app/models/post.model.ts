export class PostModel{
  idPost : number;
  createDate : Date;
  editDate : Date;
  title : string;
  content : string;
  photoId : string;
  author : string;

  constructor(id, title, content, photoId, author){
    this.idPost = id;
    this.title = title;
    this.content = content;
    this.photoId = photoId;
    this.author = author;
    this.createDate = new Date();
  }
}
