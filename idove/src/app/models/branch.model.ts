export class BranchModel {
  idBranch : number = null;
  regionId : number = null;
  calculatorId : number = null;
  name : string = null;
  branchNumber : string = null;

  constructor(idBranch,  regionId,  calculatorId,  name,   branchNumber ) {
    this.idBranch  = idBranch ;
    this.regionId  = regionId ;
    this.calculatorId  = calculatorId ;
    this.name  = name ;
    this.branchNumber  = branchNumber ;


  }

}
