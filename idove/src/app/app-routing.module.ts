import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./components/open-endpoints/login/login.component";
import {RegistrationComponent} from "./components/open-endpoints/registration/registration.component";
import {MainpageComponent} from "./components/general/mainpage/mainpage.component";
import {BasicGuard} from "./guards/basic.guard";
import {BranchPageComponent} from "./components/branch/branch-page/branch-page.component";
import {CalendarComponent} from "./components/branch/branch-administration/calendar/calendar.component";
import {ConfirmAccountComponent} from "./components/open-endpoints/confirm-account/confirm-account.component";
import {SettingsComponent} from "./components/general/settings/settings.component";
import {ResetPasswordComponent} from "./components/open-endpoints/reset-password/reset-password.component";
import {DovecoteComponent} from "./components/breeding/dovecote/dovecote.component";
import {ChangeEmailComponent} from "./components/open-endpoints/change-email/change-email.component";
import {PigeonComponent} from "./components/breeding/pigeon/pigeon.component";
import {FriendsComponent} from "./components/general/friends/friends.component";
import {AuctionsComponent} from "./components/general/auctions/auctions.component";
import {BranchMissingComponent} from "./components/general/branch-missing/branch-missing.component";
import {ProfileComponent} from "./components/general/profile/profile.component";
import {BranchAdministrationComponent} from "./components/branch/branch-administration/branch-administration.component";
import {BranchInfoComponent} from "./components/branch/branch-administration/branch-info/branch-info.component";
import {FanciersComponent} from "./components/branch/branch-administration/fanciers/fanciers.component";
import {SectionsComponent} from "./components/branch/branch-administration/sections/sections.component";
import {LineageComponent} from "./components/breeding/lineage/lineage.component";
import {LostPigeonComponent} from "./components/open-endpoints/lost-pigeon/lost-pigeon.component";
import {UserAuctionsComponent} from "./components/general/auctions/user-auctions/user-auctions.component";
import {AllAuctionsComponent} from "./components/general/auctions/all-auctions/all-auctions.component";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'confirm', component: ConfirmAccountComponent },
  { path: 'change-password', component: ResetPasswordComponent},
  { path: 'change-email', component: ChangeEmailComponent},
  { path: 'mainpage', canActivate : [BasicGuard], component: MainpageComponent },
  { path: 'friends', canActivate : [BasicGuard], component: FriendsComponent },
  { path: 'calendar', canActivate : [BasicGuard], component: CalendarComponent },
  { path: 'branch-missing', canActivate : [BasicGuard], component: BranchMissingComponent },
  { path: 'settings', canActivate : [BasicGuard], component: SettingsComponent },
  { path: 'dovecote', canActivate : [BasicGuard], component: DovecoteComponent },
  { path: 'lineage', canActivate : [BasicGuard], component: LineageComponent },
  { path: 'pigeon', canActivate : [BasicGuard], component: PigeonComponent },
  { path: 'branch/:idBranch', canActivate : [BasicGuard], component: BranchPageComponent},
  { path: 'profile/:idUser', canActivate : [BasicGuard], component: ProfileComponent},
  { path: 'lost-pigeon', component: LostPigeonComponent},
  { path: 'auctions', canActivate : [BasicGuard], component: AuctionsComponent,children:[
      { path: '', redirectTo: 'all',canActivate : [BasicGuard], pathMatch: 'full' },
      { path: 'all', canActivate : [BasicGuard], component: AllAuctionsComponent},
      { path: 'user', canActivate : [BasicGuard], component: UserAuctionsComponent },
    ]
  },
  { path: 'branch-administration/:idBranch', canActivate : [BasicGuard], component: BranchAdministrationComponent, children:[
      { path: '', redirectTo: 'branch-info',canActivate : [BasicGuard], pathMatch: 'full' },
      { path: 'branch-info',canActivate : [BasicGuard], component: BranchInfoComponent },
      { path: 'calendar',canActivate : [BasicGuard], component: CalendarComponent },
      { path: 'fanciers',canActivate : [BasicGuard], component: FanciersComponent },
      { path: 'sections',canActivate : [BasicGuard], component: SectionsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

