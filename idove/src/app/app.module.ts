import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/open-endpoints/login/login.component';
import {ButtonModule} from "primeng/button";
import { RegistrationComponent } from './components/open-endpoints/registration/registration.component';
import { MainpageComponent } from './components/general/mainpage/mainpage.component';
import {FormsModule} from "@angular/forms";
import {UserService} from "./services/user.service";
import {DialogModule} from "primeng/dialog";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  AutoCompleteModule,
  CalendarModule,
  CardModule, DropdownModule, FieldsetModule,
  InputMaskModule, InputTextareaModule,
  InputTextModule, KeyFilterModule,
  MessageService, OrganizationChartModule, OverlayPanelModule,
  PanelMenuModule,
  PasswordModule, ProgressSpinnerModule, SelectButtonModule, SpinnerModule, TabViewModule, ToggleButtonModule,
  TooltipModule,
} from "primeng/primeng";
import { MenuComponent } from './components/general/menu/menu.component';
import {HttpService} from "./services/http.service";
import {HttpClientModule} from "@angular/common/http";
import { BranchPageComponent } from './components/branch/branch-page/branch-page.component';
import { CalendarComponent } from './components/branch/branch-administration/calendar/calendar.component';
import {FullCalendarModule} from "primeng/fullcalendar";
import { CookieService } from 'ngx-cookie-service';
import {RegistrationService} from "./services/registration.service";
import { ConfirmAccountComponent } from './components/open-endpoints/confirm-account/confirm-account.component';
import {ToastModule} from "primeng/toast";
import {PopupService} from "./services/popup.service";
import { SettingsComponent } from './components/general/settings/settings.component';
import { ResetPasswordComponent } from './components/open-endpoints/reset-password/reset-password.component';
import { DovecoteComponent } from './components/breeding/dovecote/dovecote.component';
import {LeafletModule} from "@asymmetrik/ngx-leaflet";
import { ChangeEmailComponent } from './components/open-endpoints/change-email/change-email.component';
import { ProfileComponent } from './components/general/profile/profile.component';
import { PigeonComponent } from './components/breeding/pigeon/pigeon.component';
import {DataViewModule} from "primeng/dataview";
import {TableModule} from "primeng/table";
import {FancierService} from "./services/fancier.service";
import {ColorService} from "./services/color.service";
import { BranchSearchingComponent } from './components/general/branch-searching/branch-searching.component';
import { AuctionsComponent } from './components/general/auctions/auctions.component';
import { FriendsComponent } from './components/general/friends/friends.component';
import {BranchService} from "./services/branch.service";
import {CountryService} from "./services/country.service";
import {RegionService} from "./services/region.service";
import { BranchMissingComponent } from './components/general/branch-missing/branch-missing.component';
import { SectionsComponent } from './components/branch/branch-administration/sections/sections.component';
import { BranchAdministrationComponent } from './components/branch/branch-administration/branch-administration.component';
import { BranchInfoComponent } from './components/branch/branch-administration/branch-info/branch-info.component';
import { FanciersComponent } from './components/branch/branch-administration/fanciers/fanciers.component';
import { LineageComponent } from './components/breeding/lineage/lineage.component';
import { PigeonProfileComponent } from './components/breeding/pigeon/pigeon-profile/pigeon-profile.component';
import { LostPigeonComponent } from './components/open-endpoints/lost-pigeon/lost-pigeon.component';
import { AuctionsListComponent } from './components/general/auctions/auctions-list/auctions-list.component';
import { UserAuctionsComponent } from './components/general/auctions/user-auctions/user-auctions.component';
import { AllAuctionsComponent } from './components/general/auctions/all-auctions/all-auctions.component';
import {PostsComponent} from "./components/branch/branch-administration/posts/posts.component";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    MainpageComponent,
    MenuComponent,
    BranchPageComponent,
    CalendarComponent,
    ConfirmAccountComponent,
    SettingsComponent,
    ResetPasswordComponent,
    DovecoteComponent,
    ChangeEmailComponent,
    ProfileComponent,
    PigeonComponent,
    BranchSearchingComponent,
    AuctionsComponent,
    FriendsComponent,
    BranchMissingComponent,
    SectionsComponent,
    BranchAdministrationComponent,
    BranchInfoComponent,
    FanciersComponent,
    LineageComponent,
    PigeonProfileComponent,
    LostPigeonComponent,
    AuctionsListComponent,
    UserAuctionsComponent,
    AllAuctionsComponent,
    PostsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    //PrimeNG
    ButtonModule, DialogModule,  PasswordModule, InputTextModule, CardModule, PanelMenuModule, InputTextareaModule, TooltipModule,
    FullCalendarModule, CalendarModule, ToastModule, ProgressSpinnerModule, InputMaskModule, DataViewModule, TableModule,
    AutoCompleteModule, DropdownModule, ToggleButtonModule, TabViewModule, FieldsetModule, OverlayPanelModule,OrganizationChartModule,
    SelectButtonModule, KeyFilterModule, SpinnerModule,
    //Leaflet
    LeafletModule.forRoot()

  ],
  providers: [UserService, HttpService, CookieService, RegistrationService, MessageService, PopupService, FancierService, ColorService, BranchService, CountryService, RegionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
