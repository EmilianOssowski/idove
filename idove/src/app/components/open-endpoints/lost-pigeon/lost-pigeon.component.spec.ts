import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LostPigeonComponent } from './lost-pigeon.component';

describe('LostPigeonComponent', () => {
  let component: LostPigeonComponent;
  let fixture: ComponentFixture<LostPigeonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostPigeonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostPigeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
