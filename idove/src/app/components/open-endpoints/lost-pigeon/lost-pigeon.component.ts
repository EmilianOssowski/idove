import {Component, Input, OnInit} from '@angular/core';
import {PopupService} from "../../../services/popup.service";
import {UserService} from "../../../services/user.service";
import {HttpService} from "../../../services/http.service";

@Component({
  selector: 'app-lost-pigeon',
  templateUrl: './lost-pigeon.component.html',
  styleUrls: ['./lost-pigeon.component.css']
})
export class LostPigeonComponent implements OnInit {


  ringNumber: string = '';
  regex = new RegExp('^[a-zA-Z]+-[0-9]+-[0-9]+-[0-9]+$');

  firstName = '';
  lastName = '';
  email = '';
  telephoneNumber = '';

  isSending =false;
  constructor(private user: UserService, private http : HttpService, private popup : PopupService) { }

  checkRingNumber(){
    return this.regex.test(this.ringNumber.toUpperCase());
  }
  ngOnInit() {
  }

  findLostPigeon(){
    this.isSending = true;
    if(this.checkRingNumber()) {
      let body;
      if(this.user.isUserLogged){
        body = {
          ringNumber : this.ringNumber.toUpperCase(),
          firstName : this.user.userDetails.firstName,
          lastName : this.user.userDetails.lastName,
          email: this.user.mail,
          telephoneNumber : this.user.userDetails.telephoneNumber
        }
      }else {
        body = {
          ringNumber: this.ringNumber.toUpperCase(),
          firstName: this.firstName,
          lastName: this.lastName,
          email: this.email,
          telephoneNumber: this.telephoneNumber,
        };
      }
      if(
        body.email != null &&body.email != '' &&
        body.firstName !=null&&body.firstName !='' &&
        body.lastName != null&&body.lastName != '' &&
        body.telephoneNumber != null &&body.telephoneNumber != ''){
      this.http.findLostPigeon(body).subscribe((res : string)=>{
          if(res == 'EMAIL_SENT_TO_USER'){
            this.popup.addInfo('Znaleziono właściciela, wysłano powiadomienie z danymi kontaktowymi','Informacja',10000);
            this.isSending = false
          }
          else if(res == 'EMAIL_SENT_TO_ADMIN'){
            this.popup.addInfo('Nie znaleziono właściciela, ale znaleziono administratora strony oddziału i wysłano powiadomienie z danymi kontaktowymi', 'Informacja',10000)
            this.isSending = false
          }
          else if(res == 'CANNOT_FIND_ADMIN'){
            this.popup.addInfo('Niestety, nie udało nam się znaleźć informacji o gołębiu w systemie iDove','Informacja',10000);
            this.isSending = false
          }
        },err=>{
          this.popup.addErrorResponse(err.error)
        this.isSending = false
        })
      }else {
        this.popup.addError('Przed wysłaniem zgłoszenia należy uzupełnić wszystkie dane')
        this.isSending = false
      }
    }else{
      this.popup.addError('Nieprawidłowy format obrączki')
      this.isSending = false
    }
  }
  onEnter(event){
    if(event.which == 13 || event.keyCode == 13){
      this.findLostPigeon()
    }
  }
}
