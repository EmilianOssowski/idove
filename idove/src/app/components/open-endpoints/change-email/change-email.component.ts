import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpService} from "../../../services/http.service";
import {PopupService} from "../../../services/popup.service";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.css']
})
export class ChangeEmailComponent implements OnInit {
  token;

  constructor(private route: ActivatedRoute, private http: HttpService, private router: Router, private popup: PopupService, private user : UserService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  establishNewEmail() {

    this.http.establishNewEmail(this.token).subscribe(res => {
      this.popup.addInfo('Adres e-mail został zmieniony');
      this.user.logout();
      this.router.navigate(['/login'])
    }, err => {
      if(err.error =='NOT_FOUND'){
        this.popup.addInfo('Nie znaleziono adresu email')
      }
      else
      this.popup.addErrorResponse(err.error)
    })
  }




}
