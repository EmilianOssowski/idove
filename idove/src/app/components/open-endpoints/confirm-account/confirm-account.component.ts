import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {PopupService} from "../../../services/popup.service";
import {HttpService} from "../../../services/http.service";

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent implements OnInit {
  token;
  constructor(private route: ActivatedRoute, private http : HttpService, private router : Router, private popup : PopupService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
      this.confirmEmail();
    });


  }

  confirmEmail() {
    this.http.confirmAccount(this.token).subscribe(
      () => {
          this.router.navigate(['/mainpage']);
        },
        (err : HttpErrorResponse) => {
          if(err.error=='NOT_FOUND'){
            this.popup.addError('Konto zostało aktywowane lub token uległ przedawnieniu')
          }
          this.router.navigate(['/login']);
        });
  }

}
