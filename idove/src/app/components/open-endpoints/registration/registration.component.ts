import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import {RegistrationService} from "../../../services/registration.service";
import {UserModel} from "../../../models/user.model";
import {ValidatorService} from "../../../services/validator.service";
import {PopupService} from "../../../services/popup.service";


@Component({
  selector: 'app-register',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  //inputs
  mail = '';
  login = '';
  password = '';
  confirmPassword = '';
  firstName = '';
  lastName = '';
  address = '';
  city = '';
  telephoneNumber = '';

  isRegisterButtonDisabled = false;

  constructor(private registration : RegistrationService, private router : Router, private validator : ValidatorService, private popup : PopupService) { }

  ngOnInit() {

  }
  register(){
    this.isRegisterButtonDisabled = true;
    if (this.password == this.confirmPassword) {
      if(this.firstName != '' && this.firstName != null &&
        this.lastName != '' && this.lastName != null &&
        this.address != '' && this.address != null &&
        this.city != '' && this.city != null && this.telephoneNumber.length < 15 &&
      this.validator.isPasswordValid(this.password)) {
        let user: UserModel = new UserModel(this.mail,  this.password, this.firstName, this.lastName, this.address, this.city, this.telephoneNumber);
        this.registration.register(user);
      }else if(!this.validator.isPasswordValid(this.password)){
        this.popup.addError(
          'Hasło powinno posiadać od 6 do 18 znaków i może zawierać znaki specjalne "_" i "-"',   'Niepoprawne dane rejestracji')
      }else if(this.telephoneNumber.length >=15) {
        this.popup.addError('Numer telefonu jest za długi')
      }
    }
    else {
      this.popup.addError('Hasła się nie zgadzają');
    }
    this.isRegisterButtonDisabled = false;
  }
  onEnter(event){
    if(event.which == 13 || event.keyCode == 13){
      this.register()
    }
  }

}
