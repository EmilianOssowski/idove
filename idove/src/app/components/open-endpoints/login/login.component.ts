import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../services/user.service";
import {HttpService} from "../../../services/http.service";
import {PopupService} from "../../../services/popup.service";
import {CookieService} from "ngx-cookie-service";
import {BranchAdministratorService} from "../../../services/branch-administrator.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : string = 'lvzik12@gmail.com';
  password : string = 'bcrypt';
  // username : string = '';
  // password : string = '';
  mail;
  constructor(private router : Router, private user : UserService, private http : HttpService, private popup : PopupService, private cookie : CookieService, private branchAdministrator : BranchAdministratorService) { }

  ngOnInit() {
    this.user.isRestoringSession = true;
    this.tryLoginByLocalStorage();
  }
  tryLoginByLocalStorage(){
    if(this.cookie.check('access_token')){
      this.user.getUserDetails();
    }else {
      this.user.isRestoringSession = false;
    }
  }

  login(){
    if(this.username != null && this.username != '' && this.password!=null && this.password != '') {
      try {
        this.user.login(this.username, this.password)
      } catch (e) {
        console.log("Błąd")
      }
    }else{
      this.popup.addError('Podaj login i hasło')
    }

  }
  isPasswordReseting = false;
  enablePasswordForgot(){
    this.isPasswordReseting = true
  }
  resetPassword(){
    if(this.mail !=null && this.mail !='') {
      this.http.resetPassword(this.mail).subscribe(res => {
        this.popup.addInfo('Wysłano link do zmiany hasła na podany adres');
        this.isPasswordReseting = false;
      }, err => {
        this.popup.addErrorResponse(err.error)
      })
    }else{
      this.popup.addError('Podaj adres e-mail');
    }
  }

  openLostPigeon(){
    this.router.navigate(['/lost-pigeon'])
  }
  onEnter(event){
    if(event.which == 13 || event.keyCode == 13){
      this.login()
    }
  }
  onPasswordResetingEnter(event){
    if(event.which == 13 || event.keyCode == 13){
      this.resetPassword()
    }
  }
}
