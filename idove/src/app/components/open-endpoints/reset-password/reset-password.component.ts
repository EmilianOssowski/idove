import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {ActivatedRoute, Router} from "@angular/router";
import {PopupService} from "../../../services/popup.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  token;
  newPassword;
  confirmPassword;
  isButtonDisabled = false;
  constructor(private route: ActivatedRoute, private http : HttpService, private router : Router, private popup : PopupService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  establishNewPassword(){
    this.isButtonDisabled = true;
    if(this.newPassword == this.confirmPassword) {
      this.http.establishNewPassword(this.token, this.newPassword).subscribe(res =>{
        this.popup.addInfo('Hasło zostało zmienione')
        this.router.navigate(['/login'])
        this.isButtonDisabled = false;
      }, err=>{
        this.popup.addErrorResponse(err.error)
        this.isButtonDisabled = false;
      })
    }
  }
  onEnter(event){
    if(event.which == 13 || event.keyCode == 13){
      this.establishNewPassword()
    }
  }
}
