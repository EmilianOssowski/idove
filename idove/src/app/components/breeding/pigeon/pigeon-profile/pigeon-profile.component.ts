import {Component, Input, OnInit} from '@angular/core';
import {PigeonModel} from "../../../../models/pigeon.model";
import {ColorService} from "../../../../services/color.service";
import {CountryService} from "../../../../services/country.service";
import {ColorModel} from "../../../../models/color.model";
import {CountryModel} from "../../../../models/country.model";

@Component({
  selector: 'app-pigeon-profile',
  templateUrl: './pigeon-profile.component.html',
  styleUrls: ['./pigeon-profile.component.css']
})
export class PigeonProfileComponent implements OnInit {

  @Input() pigeon : PigeonModel;
  @Input() isLineage : boolean = false;
  constructor(private colorService : ColorService, private countryService : CountryService) { }

  color : ColorModel;
  country : CountryModel;
  ngOnInit() {
    this.color = this.colorService.getColor(this.pigeon.colorId);
    this.countryService.getCountryById(this.pigeon.countryId).subscribe((res : CountryModel)=>{
      this.country = res;
    })
  }

}
