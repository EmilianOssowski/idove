import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PigeonProfileComponent } from './pigeon-profile.component';

describe('PigeonProfileComponent', () => {
  let component: PigeonProfileComponent;
  let fixture: ComponentFixture<PigeonProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PigeonProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PigeonProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
