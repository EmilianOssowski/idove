import {Component, OnInit} from '@angular/core';
import {FancierService} from "../../../services/fancier.service";
import {UserService} from "../../../services/user.service";
import {PigeonModel} from "../../../models/pigeon.model";
import {CountryModel} from "../../../models/country.model";
import {ColorService} from "../../../services/color.service";
import {CountryService} from "../../../services/country.service";
import {SelectItem} from "primeng/api";
import {PopupService} from "../../../services/popup.service";
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import {ColorModel} from "../../../models/color.model";

@Component({
  selector: 'app-pigeon',
  templateUrl: './pigeon.component.html',
  styleUrls: ['./pigeon.component.css']
})
export class PigeonComponent implements OnInit {

  pigeons: Array<PigeonModel> = [];
  countryResults: Array<CountryModel>;
  colorList: SelectItem[] = [];

  constructor(private fancier: FancierService, private user: UserService, private colorService: ColorService, private countryService: CountryService, private popup: PopupService) {
  }

  ngOnInit() {

    this.fancier.pigeons.subscribe((res: Array<PigeonModel>) => {
      this.pigeons = res;
      this.getFilterBranchOptions();
    });
    this.countryService.countriesResults.subscribe(res => {
      this.countryResults = res
    });
    this.colorService.colorList.subscribe((res) => {
      for (let color of res) {
        this.colorList.push({label: color.name, value: color})
      }
    });
    this.fancier.getPigeons();
  }

  showModal = false;
  isAdding = false;
  isEditing = false;
  title = '';

  //inputs
  idPigeon;
  fancierId;
  country: CountryModel;
  color;
  branchNumber;
  yearbook: string;
  number;
  isGMP = false;
  isMP = false;
  isIMP = false;
  description = '';
  isAlive = true;
  motherId;
  fatherId;
  sex;

  sexOptions: SelectItem[] = [
    {label: 'samiec', value: true},
    {label: 'samica', value: false}
  ];
  statusOptions = [
    {label: 'obecny', value: true},
    {label: 'nieobecny', value: false}];

  yearRegex = '/[0-9]{4}';
  selectedFather: PigeonModel;
  selectedMother: PigeonModel;
  fatherResults: PigeonModel[] = [];
  motherResults: PigeonModel[] = [];

  filterBranchOptions: SelectItem[] =[];
  isSorting = false;
  filterSex = [false, true];
  filterStatus = [false, true];
  filterColor : ColorModel = null;
  filterYearbook : string = null;
  filterBranch = null;



  showAdding() {
    this.color = this.colorService.getColor(1);
    this.selectedFather = null;
    this.selectedMother = null;
    this.idPigeon = null;
    this.fancierId = null;
    this.country = null;
    //this.color = null;
    this.branchNumber = null;
    this.yearbook = '';
    this.number = null;
    this.isGMP = false;
    this.isMP = false;
    this.isIMP = false;
    this.description = '';
    this.isAlive = true;
    this.motherId = null;
    this.fatherId = null;
    this.sex = false;

    this.title = 'Dodawanie gołębia';
    this.isEditing = false;
    this.isAdding = true;
    this.showModal = true;
  }

  showEditing(pigeon: PigeonModel) {
    let color = this.colorService.getColor(pigeon.colorId);
    this.countryService.getCountryById(pigeon.countryId).subscribe((res: CountryModel) => {
      this.country = res;
    });
    this.idPigeon = pigeon.idPigeon;
    this.fancierId = pigeon.fancierId;
    this.color = color;
    this.branchNumber = pigeon.branchNumber;
    this.yearbook = pigeon.yearbook;
    this.number = pigeon.number;
    this.isGMP = pigeon.isGMP;
    this.isMP = pigeon.isMP;
    this.isIMP = pigeon.isIMP;
    this.description = pigeon.description;
    this.isAlive = pigeon.isAlive;
    this.motherId = pigeon.motherId;
    if (pigeon.motherId != null) {
      this.fancier.getPigeonByIdPigeon(pigeon.motherId).subscribe((res: PigeonModel) => {
        this.selectedMother = res;
        console.log(this.selectedMother)
      });
    } else {
      this.selectedMother = null;
    }
    this.fatherId = pigeon.fatherId;
    if (pigeon.fatherId != null) {

      this.fancier.getPigeonByIdPigeon(pigeon.fatherId).subscribe((res: PigeonModel) => {
        this.selectedFather = res;
      });
    } else {
      this.selectedFather = null;
    }
    this.sex = pigeon.sex;

    this.title = 'Edycja gołębia ' + pigeon.ringNumber;
    this.isEditing = true;
    this.isAdding = false;
    this.showModal = true;
  }

  deleting = [];

  showDeleting(idPigeon) {
    this.deleting = idPigeon;
    setTimeout(() => {
      this.deleting = null;
    }, 3000)
  }



  addPigeon() {
    if (this.yearbook.length == 4 && this.color != null && this.country != null && this.number.length < 5 && this.number != null && this.number != '' && this.checkIsOlderThanParents()) {
      let pigeon: PigeonModel = new PigeonModel(null, null, null, null, null, null, null, null);
      pigeon.idPigeon = this.idPigeon;
      pigeon.yearbook = this.yearbook;
      pigeon.number = this.number;
      pigeon.branchNumber = this.branchNumber;
      pigeon.description = this.description;
      pigeon.colorId = this.color.idColor;
      pigeon.countryId = this.country.idCountry;
      pigeon.isGMP = this.isGMP;
      pigeon.isMP = this.isMP;
      pigeon.isGMP = this.isIMP;
      pigeon.isAlive = this.isAlive;
      if (this.selectedMother != null)
        pigeon.motherId = this.selectedMother.idPigeon;
      if (this.selectedFather != null)
        pigeon.fatherId = this.selectedFather.idPigeon;
      pigeon.sex = this.sex;
      this.fancier.addPigeon(pigeon);
      this.isAdding = false;
      this.showModal = false;
    } else {
      //tu dodać ify zwiazane z poszczegółnymi błedami
      this.popup.addError('Błędne dane');
    }
  }

  editPigeon() {
    if (this.yearbook.length == 4 && this.color != null && this.country != null && this.number.length < 5 && this.number != null && this.number != '' && this.checkIsOlderThanParents()) {
      let pigeon: PigeonModel = new PigeonModel(null, null, null, null, null, null, null, null);
      pigeon.idPigeon = this.idPigeon;
      pigeon.yearbook = this.yearbook;
      pigeon.number = this.number;
      pigeon.branchNumber = this.branchNumber;
      pigeon.description = this.description;
      pigeon.colorId = this.color.idColor;
      pigeon.countryId = this.country.idCountry;
      pigeon.isGMP = this.isGMP;
      pigeon.isMP = this.isMP;
      pigeon.isGMP = this.isIMP;
      pigeon.isAlive = this.isAlive;
      if (this.selectedMother != null)
        pigeon.motherId = this.selectedMother.idPigeon;
      if (this.selectedFather != null)
        pigeon.fatherId = this.selectedFather.idPigeon;
      pigeon.sex = this.sex;
      this.fancier.editPigeon(pigeon);
      this.isAdding = false;
      this.showModal = false;
    } else {
      //tu dodać ify zwiazane z poszczegółnymi błedami
      this.popup.addError('Błędne dane');
    }
  }

  deletePigeon(pigeon) {
    this.fancier.deletePigeon(pigeon);
  }

  isProfileOpened = false;
  pigeonProfile;

  openPigeonProfile(pigeon) {
    this.isProfileOpened = true;
    this.pigeonProfile = pigeon;
  }

  searchCountries(event) {
    this.countryService.getCountryResults(event.query)
  }

  getColorName(id): string {
    return this.colorService.getColorName(id);
  }

  searchMothers(event) {
    this.fancier.getPigeonsByQuery(event.query, 0).subscribe((res: Array<PigeonModel>) => {
      this.motherResults = [];
      if (this.yearbook != null && this.yearbook.length == 4) {
        for (let p of res) {
          if (Number(p.yearbook) < Number(this.yearbook) && p.idPigeon != this.idPigeon)
            this.motherResults.push(p);
        }
      }
      else {
        this.motherResults = res;
      }
    }, err => {
      this.popup.addError(err.error);
    });
  }

  searchFathers(event) {
    this.fancier.getPigeonsByQuery(event.query, 1).subscribe((res: Array<PigeonModel>) => {
      this.fatherResults = [];
      if (this.yearbook != null && this.yearbook.length == 4) {
        for (let p of res) {
          if (Number(p.yearbook) < Number(this.yearbook) && p.idPigeon != this.idPigeon)
            this.fatherResults.push(p);
        }
      }
      else {
        this.fatherResults = res;
      }
    }, err => {
      this.popup.addError(err.error);
    });
  }

  checkIsOlderThanParents() {
    if (this.yearbook.length == 4 && this.selectedMother != null && this.selectedMother.yearbook != null && Number(this.yearbook) < Number(this.selectedMother.yearbook) ||
      this.yearbook.length == 4 && this.selectedFather != null && this.selectedFather.yearbook != null && Number(this.yearbook) < Number(this.selectedFather.yearbook)) {
      this.popup.addError('Gołąb nie może być starszy od rodzica');
      return false;
    }
    else {
      return true;
    }
  }

  exportListToPDF() {

    let data = document.getElementById('pigeon-list');

    html2canvas(data, {
      onclone: function (document) {
        while (document.getElementById('actions') != null) {
          document.getElementById('actions').remove();
        }
        document.getElementById('actions-header').remove();
      }
    }).then(canvas => {
      var imgWidth = 295;
      var pageHeight = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf('l', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('iDove-spis-golebi-' + this.user.userDetails.firstName + '-' + this.user.userDetails.lastName + '.pdf'); // Generated PDF
    })
  }

  toogleSorting() {
    this.isSorting = !this.isSorting;
  }
  getFilterBranchOptions(){
    this.fancier.pigeons.getValue().forEach((pigeon : PigeonModel) =>{
      if(!this.filterBranchOptions.find(branch => pigeon.branchNumber === branch.value) ){
        this.filterBranchOptions.push({label : pigeon.branchNumber, value : pigeon.branchNumber})
      }
    });
  }
  filter(){
    this.pigeons = this.fancier.pigeons.getValue();

    this.filterBySex();
    this.filterByStatus();
    this.filterByColor();
    this.filterByBranch();
    this.filterByYearbook();
  }

  filterBySex(){
    if (this.filterSex.length == 0 || this.filterSex.length == 2) {
      this.filterSex = [true, false]
    }
    else {
      this.pigeons = this.pigeons.filter(pigeon =>pigeon.sex === this.filterSex[0])
    }
  }

  filterByStatus() {
    if (this.filterStatus.length == 0 || this.filterStatus.length == 2) {
      this.filterStatus = [true, false]
    } else {
      this.pigeons = this.pigeons.filter(pigeon => pigeon.isAlive === this.filterStatus[0]);
    }
  }

  filterByColor() {
    if (this.filterColor != null) {
      this.pigeons = this.pigeons.filter(pigeon => pigeon.colorId === this.filterColor.idColor);
    }
  }
  filterByBranch(){
    if (this.filterBranch != null) {
      this.pigeons = this.pigeons.filter(pigeon => pigeon.branchNumber == this.filterBranch);
    }
  }
  filterByYearbook(){
    if(this.filterYearbook != null && this.filterYearbook.length == 4){
      this.pigeons = this.pigeons.filter(pigeon => pigeon.yearbook == this.filterYearbook.toString())
    }
  }
  clearFilterYearbook(){
    this.filterYearbook = null;
    this.filter()
  }
  increaseFilterYearbook(){
    let filter = Number(this.filterYearbook);
    filter = filter + 1;
    this.filterYearbook = filter.toString();
    this.filter()
  }
  decreaseFilterYearbook(){
    let filter = Number(this.filterYearbook);
    filter = filter - 1;
    this.filterYearbook = filter.toString();
    this.filter()
  }

  clearFilter() {
    this.filterStatus = [true, false];
    this.filterSex = [true, false];
    this.filterBranch = null;
    this.filterColor= null;
    this.filterYearbook = null;
    this.pigeons = this.fancier.pigeons.getValue();
  }
}
