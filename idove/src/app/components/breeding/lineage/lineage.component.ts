import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FancierService} from "../../../services/fancier.service";
import {ColorService} from "../../../services/color.service";
import {PigeonModel} from "../../../models/pigeon.model";
import {PopupService} from "../../../services/popup.service";
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-lineage',
  templateUrl: './lineage.component.html',
  styleUrls: ['./lineage.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LineageComponent implements OnInit {
  pigeon : PigeonModel = null;
  results :PigeonModel[]= [];

  mother : PigeonModel = null;
  father : PigeonModel = null;
  fatherOfMother : PigeonModel = null;
  motherOfMother : PigeonModel = null;
  fatherOfFather : PigeonModel = null;
  motherOfFather : PigeonModel = null;
  constructor(private fancier : FancierService, private popup : PopupService, private color : ColorService) { }

  ngOnInit() {
    this.fancier.pigeons.subscribe(res=>{
      this.results = res;
    })
  }
  search(event){
    this.results = this.fancier.filterPigeonsByRingNumber(event.query);
  }


  generateLineage(pigeon) {
    this.clearLineage();
    this.pigeon = pigeon;

    if(pigeon.motherId!=null)
    this.fancier.getPigeonByIdPigeon(pigeon.motherId).subscribe((res: PigeonModel) => {
      this.mother = res;
      if(res.fatherId!=null)
      this.getFatherOfMother(res.fatherId);
      if(res.motherId!=null)
      this.getMotherOfMother(res.motherId);
    }, err => {
      this.popup.addError(err.error)
    });

    if(pigeon.fatherId!=null)
    this.fancier.getPigeonByIdPigeon(pigeon.fatherId).subscribe((res: PigeonModel) => {
      this.father = res;
      if(res.motherId!=null)
      this.getMotherOfFather(res.motherId);
      if(res.fatherId!=null)
      this.getFatherOfFather(res.fatherId);
    }, err => {
      this.popup.addError(err.error)
    });
  }
  getMotherOfMother(idMother){
    this.fancier.getPigeonByIdPigeon(idMother).subscribe((res: PigeonModel) => {
      this.motherOfMother = res;
    }, err => {
      this.popup.addError(err.error)
    });
  }
  getFatherOfMother(idMother){
    this.fancier.getPigeonByIdPigeon(idMother).subscribe((res: PigeonModel) => {
      this.fatherOfMother = res;
    }, err => {
      this.popup.addError(err.error)
    });
  }
  getMotherOfFather(idFather){
    this.fancier.getPigeonByIdPigeon(idFather).subscribe((res: PigeonModel) => {
      this.motherOfFather = res;
    }, err => {
      this.popup.addError(err.error)
    });
  }
  getFatherOfFather(idFather){
    this.fancier.getPigeonByIdPigeon(idFather).subscribe((res: PigeonModel) => {
      this.fatherOfFather = res;
    }, err => {
      this.popup.addError(err.error)
    });
  }




  hideLineage(){
    this.pigeon = null;
  }
  clearLineage(){
    this.mother = null;
    this.father = null;
    this.fatherOfMother = null;
    this.motherOfMother = null;
    this.fatherOfFather = null;
    this.motherOfFather = null;
  }

  exportLineageToPDF(){
    let data = document.getElementById('lineage');
    html2canvas(data).then(canvas =>{
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('iDove-rodowod-'+this.pigeon.ringNumber+'.pdf'); // Generated PDF
    })


  }

}
