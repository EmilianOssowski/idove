import {Component, OnInit} from '@angular/core';
import {control, latLng, marker, tileLayer} from "leaflet";
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/images/marker-icon.png';
import {UserService} from "../../../services/user.service";
import {DovecoteModel} from "../../../models/dovecote.model";
import {FancierService} from "../../../services/fancier.service";
import * as L from 'leaflet';
import {LeafletDirective} from "@asymmetrik/ngx-leaflet";
import zoom = control.zoom;
import {PopupService} from "../../../services/popup.service";


@Component({
  selector: 'app-dovecote',
  templateUrl: './dovecote.component.html',
  styleUrls: ['./dovecote.component.css']
})
export class DovecoteComponent implements OnInit {

  address = '';
  city = '';
  latitude;
  longitude;


  options = {
    layers: [
       tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
         attribution: '&copy; OpenStreetMap contributors'
       }),
       //tileLayer("https://2.base.maps.api.here.com/maptile/2.1/maptile/newest/reduced.day/{z}/{x}/{y}/512/png8?app_id=" + 'IkVG1gMYxHv0fDCrrxDl' + "&app_code=" + 'G0EaPB8vFmovxk__QLZXXw' + "&ppi=320"
       // )

    ],
    zoom: 7,
    center: latLng([52, 19])
  };
  layers = [];
  map;
  constructor(private fancier : FancierService, private popup : PopupService) {
  }

  ngOnInit() {

    this.fancier.getDovecote();
    this.fancier.addMarker.subscribe((dovecote : DovecoteModel) =>{
      this.addMarker(dovecote.latitude, dovecote.longitude, dovecote.address, dovecote.city)
    })
    this.fancier.clearMapEvent.subscribe(()=>{
      this.clearMap();
    })
    //this.layers.push(marker([this.user.dovecote.latitude,this.user.dovecote.longitude]));
  }


  showCoordinates(event) {
    console.log(event.latlng)
  }

  addMarker(lat, lng, address, city) {
    this.clearMap();
    this.layers.push(marker([lat, lng]).bindPopup('<b>Twój gołębnik: </b>'+city + ' '+ address+'<br>'
        +'<b>Szerokość geograficzna:</b> '+lat +'<br><b>Długość geograficzna:</b> '+lng)
        .openPopup()
      );
    this.centerOnDovecote()
    }

  centerOnDovecote(){
    this.map.flyTo(latLng(this.fancier.dovecote.latitude,this.fancier.dovecote.longitude),9);
    this.layers[0].openPopup()
  }
  onMapReady(map : L.Map){
    this.map = map
  }
  clearMap(){
    this.layers = [];
  }


  showModal: boolean = false;
  isAdding: boolean = false;
  isEditing: boolean = false;
  isDeleting : boolean = false;
  title = '';

  showAdding() {
    this.title = "Dodawanie gołębnika";
    this.isAdding = true;
    this.showModal = true;
  }

  showEditing() {
    this.title = "Edycja gołębnika";
    this.isEditing = true;
    this.showModal = true;
  }

  showDeleting(){
    this.isDeleting = true;
  }


  addDovecote() {
    if(
    this.address != null && this.address != '' &&
    this.city != null && this.city != '' &&
    this.latitude != null  &&
    this.longitude != null)
    {
      this.fancier.addDovecote(new DovecoteModel(null, this.address, this.city, this.latitude, this.longitude));
      this.isAdding = false;
      this.showModal = false;
    }else {
      this.popup.addError('Błędne dane')
    }
  }

  editDovecote() {
    if(this.address != null && this.address != '' &&
      this.city != null && this.city != '' &&
      this.latitude != null  &&
      this.longitude != null)
    {
      let dovecote = new DovecoteModel(null, this.address, this.city, this.latitude, this.longitude);
      this.fancier.editDovecote(dovecote);
      this.showModal = false;
      this.isEditing = false;
    }else {
      this.popup.addError('Błędne dane')
    }
  }
  deleteDovecote(){
    this.fancier.deleteDovecote();
    this.isDeleting = false;
  }
}
