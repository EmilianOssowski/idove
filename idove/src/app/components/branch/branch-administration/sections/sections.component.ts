import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BranchModel} from "../../../../models/branch.model";
import {SectionModel} from "../../../../models/section.model";
import {BranchAdministratorService} from "../../../../services/branch-administrator.service";
import {PopupService} from "../../../../services/popup.service";

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})
export class SectionsComponent implements OnInit ,OnDestroy{
  private sub: any;
  idBranch;

  sections : Array<SectionModel> = [];
  constructor(private route : ActivatedRoute, private branchAdministrator : BranchAdministratorService, private popup : PopupService) { }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.idBranch = +params["idBranch"];
      this.getSections(this.idBranch);
    });
  }
  getSections(idBranch){
    this.branchAdministrator.getSectionList(idBranch).subscribe((res:Array<SectionModel>)=>{
      this.sections = res;
      console.log(this.sections)
    },err =>{
      this.popup.addError(err.error);
    });
  }
  isAdding = false;
  isEditing = false;
  showModal = false;
  title='';
  showAdding(){
    this.name = ''
    this.isEditing = false;
    this.isAdding= true;
    this.title = 'Dodawanie sekcji';
    this.showModal = true;
  }
  showEditing(section){
    this.isAdding = false;
    this.idSectionToEdit = section.idSection;
    this.name =  section.name;
    this.isEditing= true;
    this.title = 'Zmiana nazwy sekcji';
    this.showModal = true;
  }
  idSectionToEdit : number;
  name : string = '';
  addSection(){

    if(this.name != '' && this.name != null) {
      let section = new SectionModel(null, this.idBranch, this.name);
      this.branchAdministrator.addSection(section).subscribe(() => {
          this.popup.addInfo('Sekcja została dodana');
          this.getSections(this.idBranch);
        }, err => {
          this.popup.addError(err.error);
        }
      );
      this.showModal = false;
      this.isAdding = false;
    }else {
      this.popup.addError('Wpisz nazwę sekcji')
    }

  }
  editSection(){
    if(this.name != '' && this.name != null) {
      let section = new SectionModel(this.idSectionToEdit, this.idBranch, this.name);
      this.branchAdministrator.editSection(section).subscribe(() => {
        this.popup.addInfo('Nazwa sekcji została zmieniona');
        this.getSections(this.idBranch);
      }, err => {
        this.popup.addError(err.error);
      });
      this.showModal = false;
      this.isEditing = false;
    }else {
      this.popup.addError('Wpisz nazwę sekcji')
    }
  }
  isDeleting = false;
  showDeleting(section : SectionModel){
    this.idSectionToEdit = section.idSection;
    this.isDeleting = true;
  }


  deleteSection(){
    this.branchAdministrator.deleteSection(this.idSectionToEdit).subscribe(()=>{
      this.popup.addInfo('Sekcja została usunięta');
      this.getSections(this.idBranch);
      this.isDeleting = false;
    },err =>{
      this.popup.addError(err.error);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
