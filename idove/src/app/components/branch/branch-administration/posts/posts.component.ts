import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from "../../../../services/user.service";
import {PostModel} from "../../../../models/post.model";
import {PostsService} from "../../../../services/posts.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit, OnDestroy {
  private sub: any;
  idBranch;

  title : string = 'Tytuł';
  author : string = 'Autor';
  content : string = 'Zawartość';
  photo = '';

  constructor(private user : UserService, private posts: PostsService, private route : ActivatedRoute ) { }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.idBranch = +params["idBranch"];
      console.log(this.idBranch)
    });
  }

  //Photo
  convert(event){
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        console.log(event);
        this.photo = String(reader.result);
      }
    }
  }
  clearPhoto(){
    this.photo = '';
  }

  //Title
  showTitleEdit : boolean =false;
  editTitle(){
    this.showTitleEdit = true;
    this.focusTitle();
  }
  focusTitle(){
    try {
      document.getElementById('title').focus();
    }
    catch (e) {
      setTimeout(() =>
        {this.focusTitle() },
        1);
    }
  }
  saveTitle(){
    if(this.title == '' || this.title == null || this.title == undefined) this.title = '-';
    this.showTitleEdit = false;
  }

  //Author
  showAuthorEdit : boolean = false;
  editAuthor(){
    this.showAuthorEdit = true;
    this.focusAuthor();
  }
  focusAuthor(){
    try {
      document.getElementById('author').focus();
    }
    catch (e) {
      setTimeout(() =>
        {this.focusAuthor() },
        1);
    }
  }
  saveAuthor(){
    if(this.author == '' || this.author == null || this.author == undefined) this.author = '-';
    this.showAuthorEdit = false;
  }

  //Content
  showContentEdit : boolean = false;
  editContent(){
    this.showContentEdit = true;
    this.focusContent();
  }
  focusContent(){
    try {
      document.getElementById('content').focus();
    }
    catch (e) {
      setTimeout(() =>
        {this.focusContent() },
        1);
    }
  }
  saveContent(){
    if(this.content == '' || this.content == null || this.content == undefined) this.content = '-';
    this.showContentEdit = false;
  }

  addPost(){
      let post = new PostModel(null, this.title, this.content, this.photo, this.author)
      this.posts.addPost(post);
  }

  clear(){
    this.content = 'Zawartość';
    this.title = 'Tytuł';
    this.author = 'Autor';
    this.photo = '';
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}


