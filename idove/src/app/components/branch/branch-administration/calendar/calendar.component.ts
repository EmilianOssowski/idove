import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventService} from "../../../../services/event.service";
import {EventModel} from "../../../../models/event.model";
import {Helper} from "../../../../models/helper";
import {PopupService} from "../../../../services/popup.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnDestroy {
  private sub: any;
  idBranch;

  eventsList =[];

  isEditing = false;
  isAdding = false;
  isModal = false;
  modalTitle = '';
  isDeleting = false;

  idEvent;
  startDate = new Date();
  endDate = new Date();
  title;
  pl = {
    firstDayOfWeek: 0,
    dayNames: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nd", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
    dayNamesMin: ["Nd", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
    monthNames: [ "Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień" ],
    monthNamesShort: [ "Sty", "Lut", "Mar", "Kw", "Maj", "Cze","Lip", "Sie", "Wrz", "Paź", "Lis", "Gr" ],
    today: 'Dzisiaj',
    clear: 'Wyczyść',
    dateFormat: 'mm/dd/yy'
  };

  constructor(private eventsService: EventService, private popup: PopupService,private route : ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.idBranch = +params["idBranch"];
    });
    this.eventsService.eventsObs.subscribe((events: Array<EventModel>) => {
      this.eventsList = [];
        events.forEach(e =>{
          this.eventsList.push(
          {
            "title": e.title,
            "start": new Date(e.startTime),
            "end": new Date(e.endTime),
            "id" : e.idEvent
          })
        })
      }
    );
    this.eventsService.getEvents(this.idBranch);
    this.eventsService.isEditing.subscribe((e:EventModel)=>{
      this.startDate= e.startTime;
      this.endDate= e.endTime;
      this.title= e.title;
      this.isEditing = true;
      this.idEvent = e.idEvent;
      this.showEditing();
    })
  }


  showAdding() {
    this.title=''
    this.isAdding = true;
    this.isModal = true;
    this.isEditing = false;
    this.endDate = new Date(); //zrobić dodawanie godziny do czasu końca wydarzenia
    this.startDate = new Date();
    this.modalTitle = 'Dodawanie nowego wydarzenia';
  }
  showEditing() {
    this.isEditing = true;
    this.isModal = true;
    this.isAdding = false;
    this.modalTitle = 'Edytowanie wydarzenia ' + this.title;
  }

  addEvent() {
    if (this.endDate > this.startDate ) {
      if(this.title != null && this.title != undefined && this.title != '') {
        let start = Helper.getDateString(this.startDate);
        let end = Helper.getDateString(this.endDate);
        this.eventsService.addEvent(new EventModel(this.title, start, end,  null), this.idBranch);
        this.isAdding = false;
        this.isModal = false;
        this.title = '';
        this.eventsService.getEvents(this.idBranch);
      }else{
        this.popup.addError('Musisz podać tytuł');
      }
    } else {
      this.popup.addError('Data zakończenia nie może być wcześniejsza niż rozpoczęcia');
    }
  }
  editEvent(){
    if (this.endDate > this.startDate ) {
      if(this.title != null && this.title != undefined && this.title != '') {
        let start = Helper.getDateString(this.startDate);
        let end = Helper.getDateString(this.endDate);
        let ev : EventModel = new EventModel(this.title, this.startDate, this.endDate, this.idEvent);
        this.eventsService.editEvent(ev, this.idBranch);
        this.isModal = false;
        this.isEditing = false;
        this.title = '';
      }else{
        this.popup.addError('Musisz podać tytuł');
      }
    } else {
      this.popup.addError('Data zakończenia nie może być wcześniejsza niż rozpoczęcia');
    }
  }
  showDeleting(){
    this.isDeleting = true;
    setTimeout(()=>{
      this.isDeleting = false;
    },3000)

  }
  deleteEvent(){
    this.eventsService.deleteEvent(this.idEvent, this.idBranch)
    this.isDeleting = false;
    this.isModal = false;
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }



}
