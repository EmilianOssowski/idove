import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BranchModel} from "../../../../models/branch.model";

import {BranchAdministratorService} from "../../../../services/branch-administrator.service";
import {RegionModel} from "../../../../models/region.model";
import {PopupService} from "../../../../services/popup.service";


@Component({
  selector: 'app-branch-info',
  templateUrl: './branch-info.component.html',
  styleUrls: ['./branch-info.component.css']
})
export class BranchInfoComponent implements OnInit, OnDestroy {
  private sub: any;
  idBranch;
  region : RegionModel = new RegionModel(null,null,null);
  branch: BranchModel = new BranchModel(null,null,null,null,null);
  constructor(private route : ActivatedRoute, private branchAdministrator : BranchAdministratorService, private popup : PopupService) { }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.idBranch = +params["idBranch"];
      this.branchAdministrator.getBranchById(this.idBranch).subscribe((res: BranchModel)=>{
        this.branch = res;
      },err=>{
        this.popup.addErrorResponse(err.error)
      })
      ;
      this.branchAdministrator.getRegionByIdBranch(this.idBranch).subscribe((res : RegionModel)=>{
        this.region = res;
      },err=>{
        this.popup.addErrorResponse(err.error)
      })
    });

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
