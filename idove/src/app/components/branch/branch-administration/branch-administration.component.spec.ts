import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchAdministrationComponent } from './branch-administration.component';

describe('BranchAdministrationComponent', () => {
  let component: BranchAdministrationComponent;
  let fixture: ComponentFixture<BranchAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
