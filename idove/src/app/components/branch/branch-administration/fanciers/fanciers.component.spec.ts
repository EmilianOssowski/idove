import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FanciersComponent } from './fanciers.component';

describe('FanciersComponent', () => {
  let component: FanciersComponent;
  let fixture: ComponentFixture<FanciersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FanciersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FanciersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
