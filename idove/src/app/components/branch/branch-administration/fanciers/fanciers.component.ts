import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserModel} from "../../../../models/user.model";
import {BranchAdministratorService} from "../../../../services/branch-administrator.service";

@Component({
  selector: 'app-fanciers',
  templateUrl: './fanciers.component.html',
  styleUrls: ['./fanciers.component.css']
})
export class FanciersComponent implements OnInit , OnDestroy{
  private sub: any;
  idBranch;

  fanciers: Array<UserModel> = [];
  constructor(private route : ActivatedRoute, private branchAdministrator : BranchAdministratorService, private router : Router) { }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.idBranch = +params["idBranch"];
      this.branchAdministrator.getFanciersByIdBranch(this.idBranch).subscribe((res: Array<UserModel>)=>{
        this.fanciers = res;
      })
    });
  }
  openUserProfile(fancier){
    this.router.navigate(['/profile',fancier.idUser])
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
