import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BranchModel} from "../../../models/branch.model";
import {BranchService} from "../../../services/branch.service";
import {PopupService} from "../../../services/popup.service";

@Component({
  selector: 'app-branch-administration',
  templateUrl: './branch-administration.component.html',
  styleUrls: ['./branch-administration.component.css']
})
export class BranchAdministrationComponent implements OnInit {
  private sub: any;
  branch : BranchModel = new BranchModel(null,null,null,null,null)
  constructor(private route : ActivatedRoute, private branchService : BranchService, private popup : PopupService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let idBranch = +params["idBranch"];
      this.branchService.getBranch(idBranch).subscribe((res : BranchModel)=>{
        this.branch = res;
      },err=>{
        this.popup.addError(err.error);
      })
    });
  }

}
