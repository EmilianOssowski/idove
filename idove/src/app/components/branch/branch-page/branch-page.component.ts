import {Component, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventService} from "../../../services/event.service";
import {SectionModel} from "../../../models/section.model";
import {BranchService} from "../../../services/branch.service";
import {FancierService} from "../../../services/fancier.service";
import {UserService} from "../../../services/user.service";
import {UserModel} from "../../../models/user.model";

@Component({
  selector: 'app-branch-page',
  templateUrl: './branch-page.component.html',
  styleUrls: ['./branch-page.component.css']
})
export class BranchPageComponent implements OnInit, OnDestroy, OnChanges{
  private sub: any;
  idBranch;

  eventsList = [];
  fancierList : Array<UserModel> = [];
  sectionsList : Array<SectionModel> = [];
  administrator : UserModel = null;

  isApplicationSending = false;

  constructor( private route: ActivatedRoute, private branchService : BranchService, private events : EventService, private fancier : FancierService, private user : UserService, private router : Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
        this.idBranch = +params["idBranch"];
        this.branchService.getBranchDetails(this.idBranch);
        this.branchService.getSections(this.idBranch);
        this.branchService.getFanciers(this.idBranch);
        this.branchService.getAdminstratorInfo(this.idBranch);
        this.events.getEvents(this.idBranch);
      });
    this.events.eventsObs.subscribe( (res) =>{
      this.eventsList =[];
      res.forEach(e=>{
        this.eventsList.push({
          "title": e.title,
          "start": new Date(e.startTime),
          "end": new Date(e.endTime),
          "id" : e.idEvent
        })
      })
    });


    this.branchService.sections.subscribe(res =>{
      this.sectionsList = res;
    });
    this.branchService.fanciers.subscribe(res =>{
      this.fancierList = res;
    });
    this.branchService.administrator.subscribe(res=>{
      this.administrator = res;
    })

  }
  ngOnChanges(){
  //  this.sub = this.route.params.subscribe(params => {
  //    this.idBranch = +params["idBranch"];
  //    this.branch.getBranchDetails(this.idBranch);
  //  });
  //  this.branch.getSections(this.idBranch);
  //  this.region.getRegionByIdBranch(this.idBranch);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  refreshBranch(){
    this.branchService.getBranchDetails(this.idBranch);
    this.branchService.getSections(this.idBranch);
    this.branchService.getFanciers(this.idBranch);
    this.branchService.getAdminstratorInfo(this.idBranch)
  }
  belongsToSection(section){
    if(this.user.fancier.section ==null){
      return false;
    }else if(section.idSection == this.user.fancier.section.idSection){
      return true;
    }
    return false;
  }

  openUserProfile(fancier){
    this.router.navigate(['/profile',fancier.idUser])
  }
  joinSection(section){
    this.fancier.joinSection(section)
  }
  applyForAdministrator(){
    this.branchService.applyForAdministrator(this.idBranch)
  }

}
