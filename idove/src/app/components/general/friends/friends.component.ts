import { Component, OnInit } from '@angular/core';
import {FriendsService} from "../../../services/friends.service";
import {FriendModel} from "../../../models/friend.model";
import {BranchService} from "../../../services/branch.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  constructor(private friends : FriendsService, private router : Router) { }

  ngOnInit() {
    this.friends.getFriends();
    this.friends.getInvitations();

  }

  deleting = null;
  showDeleting(idFriend){
    this.deleting = idFriend;
    setTimeout(()=>{
      this.deleting = null;
    },3000)
  }
  deleteFromFriendList(friend){
    this.friends.deleteFriend(friend);
  }

  openProfile(friend :FriendModel){
    this.router.navigate(['/profile', friend.user.idUser])
  }

  rejecting = null;
  showRejecting(idInvitation){
    this.rejecting = idInvitation;
    setTimeout(()=>{
      this.rejecting = null;
    },3000)
  }
  acceptInvitation(invitation){
    this.friends.acceptInvitation(invitation.idInvitation);
  }
  rejectInvitation(invitation){
    this.friends.rejectInvitation(invitation.idInvitation);

  }

}
