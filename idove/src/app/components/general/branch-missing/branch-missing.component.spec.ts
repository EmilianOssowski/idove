import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchMissingComponent } from './branch-missing.component';

describe('BranchMissingComponent', () => {
  let component: BranchMissingComponent;
  let fixture: ComponentFixture<BranchMissingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchMissingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchMissingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
