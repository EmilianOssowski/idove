import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DateService} from "../../../../services/date.service";
import {AuctionsInterface} from "../../../../models/auction.model";

@Component({
  selector: 'app-auctions-list',
  templateUrl: './auctions-list.component.html',
  styleUrls: ['./auctions-list.component.css']
})
export class AuctionsListComponent implements OnInit {

  @Input() isUserAuctions = false;
  @Input() auctions = null;
  @Input() title = 'Wszystkie aukcje';

  @Output() edit: EventEmitter<AuctionsInterface> = new EventEmitter();
  @Output() delete: EventEmitter<AuctionsInterface> = new EventEmitter();
  constructor(private dateService : DateService) { }

  ngOnInit() {

  }
  convertDate(date: Date){
    return this.dateService.getDateDisplay(date);
  }
  showEditing(auction){
    this.edit.emit(auction);
  }
  showDeleting(auction){
    this.delete.emit(auction)
  }

}
