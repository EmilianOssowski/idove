import { Component, OnInit } from '@angular/core';
import {AuctionsInterface} from "../../../../models/auction.model";
import {AuctionsService} from "../../../../services/auctions.service";

@Component({
  selector: 'app-all-auctions',
  templateUrl: './all-auctions.component.html',
  styleUrls: ['./all-auctions.component.css']
})
export class AllAuctionsComponent implements OnInit {
  auctions: Array<AuctionsInterface> = [];

  constructor(private auctionsService : AuctionsService) {
  }

  ngOnInit() {
    this.auctionsService.getAuctionsList();
    this.auctionsService.auctionsList.subscribe((res: Array<AuctionsInterface>) => {
      this.auctions = res;
    })
  }

}
