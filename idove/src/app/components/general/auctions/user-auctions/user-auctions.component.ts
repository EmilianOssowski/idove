import { Component, OnInit } from '@angular/core';
import {AuctionModel, AuctionsInterface} from "../../../../models/auction.model";
import {AuctionsService} from "../../../../services/auctions.service";

@Component({
  selector: 'app-user-auctions',
  templateUrl: './user-auctions.component.html',
  styleUrls: ['./user-auctions.component.css']
})
export class UserAuctionsComponent implements OnInit {

  auctions : Array<AuctionsInterface> = [];
  constructor(private auctionsService : AuctionsService) { }
  idAuction;
  title = '';
  description ='';

  isEditing = false;
  isDeleting = false;
  ngOnInit() {
    this.auctionsService.getUserAuctionsList();
    this.auctionsService.userAuctionsList.subscribe((res:Array<AuctionsInterface>)=>{
      this.auctions = res;
    })

  }
  openEdit(event){
    this.idAuction = event.auction.idAuction;
    this.title = event.auction.title;
    this.description = event.auction.description;
    this.isEditing = true;
  }
  editAuction(){
    let auction = new AuctionModel(this.idAuction, null, this.title, this.description);
    this.auctionsService.editAuction(auction);
    this.isEditing = false;
  }

  openDelete(event){
    this.idAuction = event.auction.idAuction;
    this.isDeleting = true;
  }
  deleteAuction(){
    this.auctionsService.deleteAuction(this.idAuction);
    this.isDeleting = false;
  }
}
