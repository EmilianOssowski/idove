import {Component, OnChanges, OnInit} from '@angular/core';
import {AuctionModel, AuctionsInterface} from "../../../models/auction.model";
import { AuctionsService} from "../../../services/auctions.service";

@Component({
  selector: 'app-auctions',
  templateUrl: './auctions.component.html',
  styleUrls: ['./auctions.component.css']
})
export class AuctionsComponent implements OnInit{

  auctions : Array<AuctionsInterface> = [];
  constructor(private auctionsService : AuctionsService) {
  }

  ngOnInit() {
    this.auctionsService.getAuctionsList();
    this.auctionsService.auctionsList.subscribe((res : Array<AuctionsInterface>) =>{
      this.auctions = res;
      this.sortNewestToOldest();
    })
  }

  isAdding : boolean = false;
  showAdding(){
  this.clearInputs();
    this.isAdding  = true;
  }
  sortNewestToOldest(){
    this.auctions.sort((d1,d2)=>{
      return d1>d2 ? 1 : d1<d2 ? -1 : 0;
    })
  }
  sortOldestToNewest(){
    this.auctions.sort((d1,d2)=>{
      return d1>d2 ? -1 : d1<d2 ? 1 : 0;
    })
  }
  title ='';
  description ='';
  addAuction(){
    let auction = new AuctionModel(null, null, this.title, this.description);
    this.auctionsService.addAuction(auction);
    this.isAdding = false;
  }


  clearInputs(){
    this.title = '';
    this.description ='';
  }



}
