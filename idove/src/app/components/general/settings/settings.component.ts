import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user.service";
import {HttpService} from "../../../services/http.service";
import {PopupService} from "../../../services/popup.service";
import {UserDetailsModel} from "../../../models/userDetails.model";
import {FancierService} from "../../../services/fancier.service";
import {BranchModel} from "../../../models/branch.model";
import {ValidatorService} from "../../../services/validator.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {


  //dane oddziału do którego należy użytkownik
  branch : BranchModel = new BranchModel(null,null,null,"",null);
  constructor(private user : UserService, private http: HttpService, private popup : PopupService, private fancier : FancierService, private validator : ValidatorService) { }


  ngOnInit() {
    this.user.refreshUserDetails();
    this.firstname = this.user.userDetails.firstName ;
    this.lastname = this.user.userDetails.lastName ;
    this.address = this.user.userDetails.address ;
    this.city = this.user.userDetails.city ;
    this.telephoneNumber = this.user.userDetails.telephoneNumber ;
  }

  firstname ='';
  lastname ='';
  address ='';
  city ='';
  telephoneNumber ='';
  changeDetails( ){
    if(this.firstname != '' && this.firstname != null &&
      this.lastname != '' && this.lastname != null &&
      this.address != '' && this.address != null &&
      this.city != '' && this.city != null && this.telephoneNumber.length < 15) {
      let userDetails = new UserDetailsModel(this.firstname, this.lastname, this.address, this.city, this.telephoneNumber);
      this.http.changeUserDetails(this.user.getBearerToken(), userDetails).subscribe(() => {
          this.popup.addInfo('Dane zostały zmienione');
          this.isChangingDetails = false;
          this.user.refreshUserDetails();
        }, err => {
          this.popup.addErrorResponse(err.status)
        }
      )
    }else {
      this.popup.addError("Podaj wszystkie dane")
    }
  }
  isChangingDetails = false;
  showChangeDetails() {
    this.isChangingDetails = true;
  }

  isChangingPassword = false;
  showChangePassword(){
    this.isChangingPassword = true;
  }
  changePasswordButtonBlocked = false
  oldPassword;
  newPassword;
  confirmNewPassword;
  changePassword(){
    this.oldPassword = '';
    this.newPassword = '';
    this.confirmNewPassword = '';
    this.changePasswordButtonBlocked = true;
    if(this.newPassword == this.confirmNewPassword && this.newPassword != null  && this.confirmNewPassword != null && this.oldPassword!= null && this.oldPassword != this.newPassword){
      if(this.validator.isPasswordValid(this.newPassword)) {
        this.http.changePassword(this.user.getBearerToken(), this.oldPassword, this.newPassword).subscribe(res => {
          this.popup.addInfo('Hasło zostało zmienione');
          this.isChangingPassword = false;
          this.changePasswordButtonBlocked = false;

        }, err => {
          this.popup.addErrorResponse(err.error)
          this.changePasswordButtonBlocked = false;
        })
      }else{
        this.popup.addError("Hasło powinno posiadać od 6 do 18 znaków i może zawierać znaki specjalne \"_\" i \"-\"'")
        this.changePasswordButtonBlocked = false;
      }
    }
    else if (this.newPassword == null  || this.confirmNewPassword == null){
      this.popup.addError('Hasło nie może być puste');
      this.changePasswordButtonBlocked = false;
    }else if(this.newPassword != this.confirmNewPassword){
      this.popup.addError('Hasła się nie zgadzają');
      this.changePasswordButtonBlocked = false;
    }else if(this.oldPassword == null){
      this.popup.addError('Należy podać stare hasło');
      this.changePasswordButtonBlocked = false;
    }else if(this.newPassword==this.oldPassword){
      this.popup.addError('Nowe hasło musi być inne niż stare');
      this.changePasswordButtonBlocked = false;
    } else{
      this.changePasswordButtonBlocked = false;
    }
  }

  changeEmailButtonBlocked = false;
  isChangingEmail=false;
  showChangeEmail(){
    this.isChangingEmail = true;
  }
  mail;
  changeEmail(){
    if(this.mail!= null && this.mail!= '') {
      this.changeEmailButtonBlocked = true;
      this.http.changeEmail(this.user.getBearerToken(), this.mail).subscribe(res => {
        this.popup.addInfo('Aby potwierdzić zmianę adresu e-mail, otwórz link wysłany na aktualny adres e-mail', 'Informacja', 5000)
        this.isChangingEmail = false;
        this.user.logout();
        this.changeEmailButtonBlocked = false;
      }, err => {
        this.popup.addErrorResponse(err.error)
        this.changeEmailButtonBlocked = false;
      })
    }else {
      this.popup.addError('Podaj adres e-mail')
    }
  }


  isDeletingAccount = false;
  showDeleteAccount(){
    this.isDeletingAccount = true;
  }
  password;
  deleteAccount(){
    if(this.password != null && this.password!= '') {
      this.http.deleteAccount(this.user.getBearerToken(), this.password).subscribe(res => {
        this.popup.addInfo('Konto zostało usunięte');
        this.user.logout()
      }, err => {
        this.popup.addErrorResponse(err.error);

      })
    }else{
      this.popup.addError('Podaj hasło')
    }
  }

  isLeavingSection=false;
  showLeavingSection(){
    this.isLeavingSection = true;
  }
  leaveSection(){
    this.fancier.leaveSection();
    this.isLeavingSection = false;
  }

}
