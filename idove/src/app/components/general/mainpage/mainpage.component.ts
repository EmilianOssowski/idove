import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user.service";
import {PostModel} from "../../../models/post.model";


@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {


  constructor(private user: UserService ) { }

  ngOnInit() {

  }

}
