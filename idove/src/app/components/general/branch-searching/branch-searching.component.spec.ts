import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchSearchingComponent } from './branch-searching.component';

describe('BranchSearchingComponent', () => {
  let component: BranchSearchingComponent;
  let fixture: ComponentFixture<BranchSearchingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchSearchingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchSearchingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
