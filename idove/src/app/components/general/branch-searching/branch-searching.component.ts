import {Component, Input, OnInit} from '@angular/core';
import {BranchModel} from "../../../models/branch.model";
import {BranchService} from "../../../services/branch.service";
import {Router} from "@angular/router";
import {UserModel} from "../../../models/user.model";
import {UserProfileService} from "../../../services/user-profile.service";

@Component({
  selector: 'app-branch-searching',
  templateUrl: './branch-searching.component.html',
  styleUrls: ['./branch-searching.component.css']
})
export class BranchSearchingComponent implements OnInit {
  results : Array<BranchModel>;
  query: string;
  isBranchSearching = true;
  userResults : Array<UserModel> = [];

  @Input() isHeader = true;
  constructor(private branch : BranchService, private router : Router, private userProfile : UserProfileService) { }

  ngOnInit() {
    //this.branch.getBranchesByQuery()
    this.branch.branchResults.subscribe((res : Array<BranchModel>) => {
      this.results = res;
    })
    this.userProfile.userResults.subscribe((res : Array<UserModel>)=>{
      this.userResults = res;
    })
  }
  searchBranch(event){
    this.branch.getBranchesByQuery(event.query);
  }

  goToBranchPage(event){
    this.router.navigate(['/branch', event.idBranch]);
    this.query = null;
  }

  toogleSearchSubject(){
    this.isBranchSearching = !this.isBranchSearching
  }
  searchUser(event){
    this.userProfile.getUserResults(event.query)
  }
  goToUserProfile(event){
    this.router.navigate(['/profile', event.idUser])
  }
}
