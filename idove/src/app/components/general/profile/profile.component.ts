import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserProfileService} from "../../../services/user-profile.service";
import {UserModel} from "../../../models/user.model";
import {DovecoteModel} from "../../../models/dovecote.model";
import {BranchModel} from "../../../models/branch.model";
import {FriendsService} from "../../../services/friends.service";
import {AuctionsInterface} from "../../../models/auction.model";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  private sub: any;
  idUser;

  user : UserModel = new UserModel(null,null,null,null,null,null,null);
  dovecote : DovecoteModel = new DovecoteModel(null,null,null,null,null);
  branch : BranchModel = new BranchModel(null,null,null,null,null);
  auctions : Array<AuctionsInterface> = [];

  constructor(private route: ActivatedRoute, private userProfile : UserProfileService, private friends : FriendsService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.idUser= +params["idUser"];
      this.userProfile.getUserProfile(this.idUser);
    });
    this.userProfile.branch.subscribe((res : BranchModel) =>{
      this.branch = res;
    });
    this.userProfile.user.subscribe((res : UserModel) =>{
      this.user = res;
    });
    this.userProfile.dovecote.subscribe((res : DovecoteModel) =>{
      this.dovecote = res;
    })
    this.userProfile.auctions.subscribe( (res: Array<AuctionsInterface>)=>{
      this.auctions = res;
    })
  }

  sendInvitation(){
    this.friends.sendInvitation(this.idUser);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
