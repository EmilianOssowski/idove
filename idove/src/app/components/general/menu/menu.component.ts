import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {Router} from "@angular/router";
import {BranchService} from "../../../services/branch.service";
import {BranchAdministratorService} from "../../../services/branch-administrator.service";
import {BranchModel} from "../../../models/branch.model";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  items: MenuItem [];
  constructor(private router : Router, private branch : BranchService, private branchAdministrator : BranchAdministratorService) { }

  ngOnInit() {


    this.items = [
      {
        label: 'Strona główna',
        icon: 'pi pi-pw pi-home',
        command: (click) => {
          this.router.navigate(["/mainpage"])
        }
      },
      {
        label: 'Ustawienia',
        icon: 'pi pi-pw pi-cog',
        command: (click) => {
          this.router.navigate(['/settings'])
        }
      },
      {
        label: 'Znajomi',
        icon: 'pi pi-pw pi-users',
        command: (click) => {
          this.router.navigate(['/friends'])
        }
      },

      {
        label: 'Znaleziony gołąb',
        icon: 'pi pi-pw pi-search',
        command: (click) => {
          this.router.navigate(['/lost-pigeon'])
        }
      },
      {
        label: 'Aukcje',
        icon: 'pi pi-pw pi-shopping-cart',
        items:[
          {
            label: 'Wszystkie aukcje',
            icon: 'pi pi-pw pi-shopping-cart',
            command: (click) => {
              this.router.navigate(['/auctions/all'])
            }
          },
          {
            label: 'Moje aukcje',
            icon: 'pi pi-pw pi-money-bill',
            command: (click) => {
              this.router.navigate(['/auctions/user'])
            }
          },
        ]
      },
      {
        label: 'Hodowla',
        icon: 'pi pi-pw pi-list',
        items: [
          {
            label: 'Gołębnik',
            icon: 'pi pi-pw pi-map-marker',
            command: (click) => {
              this.router.navigate(['/dovecote'])
            }
          },
          {
            label: ' Gołębie',
            icon: 'fas pw-pw fa-dove',
            command: (click) => {
              this.router.navigate(['/pigeon'])
            }
          },
          {
            label: 'Oddział',
            icon: 'pi pi-pw pi-table',
            command: (click) => {
              this.branch.openBranchSite()
            }
          },
          {
            label: 'Rodowody',
            icon: 'pi pi-pw pi-sitemap',
            command: (click) => {
              this.router.navigate(['/lineage'])
            }
          },

        ]
      },
    ];
    this.branchAdministrator.initializeBranchAdministratorMenu.subscribe((branch : BranchModel)=>{
      this.addBranchAdministratorMenu(branch);
    })
  }

  addBranchAdministratorMenu(branch){
    this.items.push({
      label: 'Oddział ' + branch.branchNumber,
      icon: 'pi pi-pw pi-list',
      items: [
        {
          label: 'Informacje',
          icon: 'pi pi-pw pi-info-circle',
          command: (click) => {
            this.router.navigate(['/branch-administration', branch.idBranch, 'branch-info'])
          }
        },
        {
          label: 'Sekcje',
          icon: 'pi pi-pw pi-clone',
          command: (click) => {
            this.router.navigate(['/branch-administration', branch.idBranch, 'sections'])
          }
        },
        // {
        //   label: 'Posty',
        //   icon: 'pi pi-pw pi-clone',
        //   command: (click) => {
        //     this.router.navigate(['/branch-administration', branch.idBranch, 'posts'])
        //   }
        // },
        {
          label: 'Terminarz',
          icon: 'pi pi-pw pi-calendar',
          command: (click) => {
            this.router.navigate(['/branch-administration', branch.idBranch, 'calendar'])
          }
        },
        {
          label: 'Hodowcy',
          icon: 'pi pi-pw pi-users',
          command: (click) => {
            this.router.navigate(['/branch-administration', branch.idBranch, 'fanciers'])
          }
        },
      ]
    },)
  }
}
